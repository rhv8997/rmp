﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem {
	public int amount;
	public bool shouldExpand;
	public GameObject PrefabObject;
}

// Reference:
//https://www.raywenderlich.com/847-object-pooling-in-unity
public class ObjectPooler : MonoBehaviour {
	public static ObjectPooler SharedInstance;
	public List<GameObject> pooled;
	public List<ObjectPoolItem> itemsToPool;

	void Awake(){
		SharedInstance = this;
	}

	// Use this for initialization
	void Start () {
		pooled = new List<GameObject> ();
		foreach (ObjectPoolItem item in itemsToPool) {
			for (int i = 0; i < item.amount; i++) {
				GameObject obj = Instantiate(item.PrefabObject) as GameObject;
				obj.SetActive(false);
				pooled.Add(obj);
			}
		}
	}

	public GameObject GetPooledObject(string tag){
		GameObject poolObj = pooled.Find (obj => !obj.activeInHierarchy && obj.tag == tag);
		foreach (ObjectPoolItem item in itemsToPool) {
			Debug.Log (item.PrefabObject.tag);
			if (item.PrefabObject.tag == tag) {
				if (item.shouldExpand) {
					poolObj = Instantiate(item.PrefabObject) as GameObject;
					poolObj.SetActive(false);
					pooled.Add(poolObj);
				}
			}
		}
		return poolObj;
	}

	public void DeactivateAll(string tag){
		foreach (GameObject obj in pooled) {
			if (obj.activeInHierarchy && obj.tag == tag){
				obj.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
