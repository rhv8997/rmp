﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitingProcessPanel : MonoBehaviour {
	public Text ProcessTitle;
	public Text NextProcessDue;
	public Text NextProcessTitle;
	public Text NextProcessStatus;
	public Text NextProcessPriority;
	public Text NextProcessPerson;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
