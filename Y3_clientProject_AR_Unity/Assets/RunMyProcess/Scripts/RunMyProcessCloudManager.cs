/*===============================================================================
Copyright (c) 2017 PTC Inc. All Rights Reserved.
 
Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using LitJson;


public class DepartmentEvent : UnityEvent<Department> {}

/*
 * Custom implmentation of CloudRecoContentManager
 * See Assets/SamplesResources/SceneAssets/CloudReco/Scripts/CloudRecoContentManager.cs
*/
public class RunMyProcessCloudManager : MonoBehaviour
{

	#region PRIVATE_MEMBER_VARIABLES

	[SerializeField] Transform CloudTarget;
	public Dictionary<string, string> departmentDict = new Dictionary<string, string>();

	Transform contentManagerParent;
	Transform currentAugmentation;
	public DepartmentEvent OnDepartmentUpdate = new DepartmentEvent ();
	#endregion // PRIVATE_MEMBER_VARIABLES
	private string domain = "http://ec2-18-130-43-56.eu-west-2.compute.amazonaws.com:4000";
	#region UNITY_MONOBEHAVIOUR_METHODS

	void Start()
	{

	}

	#endregion // UNITY_MONOBEHAVIOUR_METHODS


	#region PUBLIC_METHODS

	public void ShowTargetInfo(bool showInfo)
	{
		//Canvas canvas = EmployeeList.GetComponentInParent<Canvas>();

		//canvas.enabled = showInfo;
	}

	public void HandleTargetFinderResult(Vuforia.TargetFinder.TargetSearchResult targetSearchResult)
	{
		Debug.Log("<color=blue>HandleTargetFinderResult(): " + targetSearchResult.TargetName + "</color>");

		//Create new process
		Debug.Log(targetSearchResult.MetaData);
		var url = domain + "/api/rooms?id=" + targetSearchResult.MetaData.Trim('"');
		StartCoroutine (RequestDepartment(url, targetSearchResult.MetaData.Trim('"')));
	}

	#endregion // PUBLIC_METHODS


	#region // PRIVATE_METHODS

	// References:
	// https://stackoverflow.com/a/46008025
	// https://medium.com/@MissAmaraKay/parsing-json-in-c-unity-573d1e339b6f
	IEnumerator RequestDepartment(string uri, string id){
		UnityWebRequest request = UnityWebRequest.Get (uri);
		yield return request.SendWebRequest ();
		if (request.isNetworkError || request.isHttpError) {
			Debug.Log (request.downloadHandler.text);
		} else {
			Debug.Log (request.downloadHandler.text);
			var dep = Department.CreateFromJSON (request.downloadHandler.text);
			dep.id = id;
			OnDepartmentUpdate.Invoke (dep);
		}
	}

	#endregion // PRIVATE_METHODS
}
