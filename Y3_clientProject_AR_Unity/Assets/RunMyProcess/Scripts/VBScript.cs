﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBScript : MonoBehaviour, IVirtualButtonEventHandler 
{ 
	public GameObject vbObject; // this is our virtual button variable; 
	public Animation modelAnim; // this is our 3D model variable; 

	// Use this for initialization 
	void Start () 
	{ 
		vbObject = GameObject.Find("TitleButton"); 
		vbObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this); 
		modelAnim.GetComponent<Animation>();
		Debug.Log (vbObject.GetComponent<VirtualButtonBehaviour> ().transform);
	} 

	public void OnButtonPressed(VirtualButtonBehaviour vb) 
	{ 
		Debug.Log ("CLICKED CLIKCED");
		modelAnim.Play("Attack");   // This example has used Walk animation. You can use other available animations for the character. 
	} 

	public void OnButtonReleased(VirtualButtonBehaviour vb) 
	{ 
		Debug.Log ("NOTCLICKEDCLICKED");
		modelAnim.Play("none"); 
	} 

	// Update is called once per frame 
	void Update () 
	{ 

	} 
}