﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBTest : MonoBehaviour, IVirtualButtonEventHandler {

	public GameObject vbObject;

	// Use this for initialization
	void Start () {
		vbObject = GameObject.Find("Astro"); 
		vbObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
		Debug.Log ("Registered: " + vbObject.transform);
	}

	public void OnButtonPressed(VirtualButtonBehaviour vb) 
	{ 
		Debug.Log ("Clicked");
	} 

	public void OnButtonReleased(VirtualButtonBehaviour vb) 
	{ 
		Debug.Log ("NOTCLICKEDCLICKED");
	} 

}
