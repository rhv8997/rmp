﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Instance {
	public string id;
	public string name;
	public string status;
	public string published;
	public NextProcess nextProcess;
	public string error;
	public string person;

	public Instance(){
	}

	public DateTime ConvertedDate(){
		return DateTime.Parse(published, null, System.Globalization.DateTimeStyles.RoundtripKind);
	}

}
