﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Department {
	public string id;
	public List<Employee> employees;
	public string room;
	public List<Instance> data;
	public ClosestRoom closestRoom;

	public Department(){
	}

	public static Department CreateFromJSON(string jsonString){
		return JsonUtility.FromJson<Department> (jsonString);
	}
	
	public float ForLoop(){            
		float processTotal= 0f;
		float processPass=0f;
		float percentage=0f;

		for (int i = 0; i < data.Count; i++) {
			if (data [i].status == "201") {
				processPass = processPass + 1;
			}
			processTotal = processTotal + 1;
		}

		Debug.Log (processPass);
		percentage = processPass / processTotal;

		return percentage;
	}


}
