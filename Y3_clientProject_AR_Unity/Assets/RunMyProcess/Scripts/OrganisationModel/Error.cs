﻿using UnityEngine;
using System.Collections;

public class Error {

	public string id { get; set; }
	public string name { get; set; }
	public string status { get; set; }
	public string error { get; set; }
	public string person { get; set; }

	public Error() {
	}
}

