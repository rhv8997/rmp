﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Employee {
	public string name;
	public bool active;
	public int id;
}
