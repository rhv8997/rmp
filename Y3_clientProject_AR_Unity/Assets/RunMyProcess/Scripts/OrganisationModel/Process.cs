﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Process {
	public string name;
	public int status;
	public string DateAndTime;

	public Process(string processName, int processStatus, string datetime){
		name = processName;
		status = processStatus;
		DateAndTime = datetime;
	}

	public DateTime ConvertedDate(){
		return DateTime.Parse(DateAndTime, null, System.Globalization.DateTimeStyles.RoundtripKind);
	}

}
