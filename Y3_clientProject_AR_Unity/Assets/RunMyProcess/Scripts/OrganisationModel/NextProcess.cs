﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//class generated through json2csharp.com
[System.Serializable]
public class NextProcess
{
	public string[] name;
	public string status;
	public string priority;
	public string due;
	public string person;

	public NextProcess() {
	}

	public DateTime ConvertedDate(){
		return DateTime.Parse(due, null, System.Globalization.DateTimeStyles.RoundtripKind);
	}
		

}

