﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class DepartmentLost : UnityEvent {}

/*
 * Custom implmentation of CloudRecoTrackableEventHandler
 * See Assets/SamplesResources/SceneAssets/CloudReco/Scripts/CloudRecoTrackableEventHandler.cs
*/
public class RunMyProcessTrackableEventHandler : DefaultTrackableEventHandler
{
	#region PUBLIC_MEMBERS
	/// <summary>
	/// The scan-line rendered in overlay when Cloud Reco is in scanning mode.
	/// </summary>
	ScanLine m_ScanLine;
	RunMyProcessCloudManager m_CloudRecoContentManager;
	public DepartmentLost OnDepartmentLost = new DepartmentLost();
	#endregion // PUBLIC_MEMBERS


	#region PROTECTED_METHODS

	protected override void Start()
	{

		base.Start();

		m_ScanLine = FindObjectOfType<ScanLine>();
		m_CloudRecoContentManager = FindObjectOfType<RunMyProcessCloudManager>();
	}

	protected override void OnTrackingFound()
	{
		Debug.Log("<color=blue>OnTrackingFound()</color>");

		base.OnTrackingFound();

		if (m_CloudRecoContentManager)
		{
			m_CloudRecoContentManager.ShowTargetInfo(true);
		}

		// Stop finder since we have now a result, finder will be restarted again when we lose track of the result
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		if (objectTracker != null)
		{
			objectTracker.TargetFinder.Stop();

			if (m_ScanLine)
			{
				// Stop showing the scan-line
				m_ScanLine.ShowScanLine(false);
			}
		}
	}

	protected override void OnTrackingLost()
	{
		Debug.Log("<color=blue>OnTrackingLost()</color>");

		base.OnTrackingLost();

		if (m_CloudRecoContentManager)
		{
			m_CloudRecoContentManager.ShowTargetInfo(false);
			OnDepartmentLost.Invoke ();

		}

		// Start finder again if we lost the current trackable
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		if (objectTracker != null)
		{
			objectTracker.TargetFinder.ClearTrackables(false);
			objectTracker.TargetFinder.StartRecognition();

			if (m_ScanLine)
			{
				// Start showing the scan-line
				m_ScanLine.ShowScanLine(true);
			}
		}
	}

	#endregion //PROTECTED_METHODS
}
