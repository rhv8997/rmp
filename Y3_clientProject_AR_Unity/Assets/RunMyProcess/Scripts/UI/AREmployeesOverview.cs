﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AREmployeesOverview : DepartmentElement {
	public Text title;
	public GameObject ContentPanel;
	private GameObject[] employees;
	private static string Prefab = "EmployeesPanelImage";

	protected override void DepartmentUpdate (Department dep)
	{
		ObjectPooler.SharedInstance.DeactivateAll(Prefab);
		title.text = "Employees";
		Populate (dep.employees);
	}

	private GameObject[] Populate(List<Employee> employees){
		return employees.Select (emp => {
			GameObject employee = ObjectPooler.SharedInstance.GetPooledObject(Prefab);
			EmployeesPanelImage panel = employee.GetComponent<EmployeesPanelImage> ();
			panel.employeeNameText.text = emp.name;
			Debug.Log(panel.employeeNameText.text);
			if (emp.active){
				panel.employeeStatusImage.color = Color.green;
			} else {
				panel.employeeStatusImage.color = Color.red;
			}
			employee.transform.SetParent (ContentPanel.transform, false);
			employee.SetActive(true);
			return employee;
		}).ToArray ();
	}

	protected override void DepartmentClick ()
	{
		// for future use
		throw new System.NotImplementedException ();
	}

	protected override void DepartmentRemove ()
	{
		ObjectPooler.SharedInstance.DeactivateAll (Prefab);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
