﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ARChart : DepartmentElement {
	private GameObject currentGraphic;
	private string Prefab = "Chart";

	protected override void DepartmentUpdate(Department dep){
		Debug.Log("ARChart: " + dep.room);
		if (!string.IsNullOrEmpty(dep.room)){
			currentGraphic.GetComponent<BarChart> ().plot ("Processes",
				dep.data.Select(inst => {
					Color colour;
					if (inst.status == "201"){
						colour = Color.green;
					} else if (inst.status == "102"){
						colour = Color.yellow;
					} else {
						colour = Color.red;
					}
					return new BarChart.BarData(inst.name, 0f, inst.status, colour);
				}).ToList(), true, false);
			currentGraphic.SetActive (true); // show to user after data has been plotted for better user experience
		}
	}

	protected override void DepartmentClick(){
		Debug.Log ("org clicked!");
	}

	protected override void DepartmentRemove ()
	{
		if (currentGraphic != null) {
			// Set inactive to avoid the chart visibly changing when a new image target is recognised
			currentGraphic.SetActive (false);
		}
	}

	// Use this for initialization
	void Start () {
		// Instantiate the chart prefab at application start and set as inactive to avoid having to instantiate the
		// prefab GameObject at runtime, thus we can ensure performance is not effected
		currentGraphic = Instantiate (Resources.Load (Prefab, typeof(GameObject)), this.transform) as GameObject;
		currentGraphic.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
