﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARHealth : DepartmentElement {

	private GameObject currentGraphic;
	[HideInInspector]
	public Department graphicDep;
	private string Prefab = "Slider3D";

	protected override void DepartmentUpdate(Department dep){
		Debug.Log("ARHealth: " + dep.room);
		graphicDep = dep;
		if (!string.IsNullOrEmpty(dep.room)){
			// Get the department's health from the ForLoop method and set the value of the health bar
			currentGraphic.GetComponent<HealthUI> ().healthValue(dep.ForLoop());
			currentGraphic.transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
			currentGraphic.SetActive (true); // show to user after data has been plotted for better user experience
		}

	}

	protected override void DepartmentClick(){
		Debug.Log ("org clicked!");
	}

	protected override void DepartmentRemove ()
	{
		if (currentGraphic != null) {
			// Set inactive to avoid the chart visibly changing when a new image target is recognised
			currentGraphic.SetActive (false);
		}
	}

	// Use this for initialization
	void Start () {
		// Instantiate the chart prefab at application start and set as inactive to avoid having to instantiate the
		// prefab GameObject at runtime, thus we can ensure performance is not effected
		currentGraphic = Instantiate (Resources.Load (Prefab, typeof(GameObject)), this.transform) as GameObject;
		currentGraphic.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
