﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text.RegularExpressions;

public class ARFailedAndWaiting : DepartmentElement {
	private GameObject[] Processes;
	public GameObject WaitingContent;
	public GameObject FailedContent;
	private string WaitingPrefab = "WaitingProcessesPanel";
	private string FailedPrefab = "ErrorProcessesPanel";

	protected override void DepartmentUpdate (Department dep)
	{
		ObjectPooler.SharedInstance.DeactivateAll (WaitingPrefab);
		ObjectPooler.SharedInstance.DeactivateAll (FailedPrefab);
		List<Instance> WaitingAndFailed = dep.data.Where(inst => inst.status == "102" || inst.status == "301").ToList();
		//Processes = Populate (WaitingAndFailed);
		Populate(WaitingAndFailed);

	}

	private GameObject[] Populate(List<Instance> processes){
		return processes.Select (inst => {
			GameObject process = null;
			if (inst.status == "102") {
				// Instantiate failed
				process = ObjectPooler.SharedInstance.GetPooledObject(WaitingPrefab);
				WaitingProcessPanel panel = process.GetComponent<WaitingProcessPanel>();
				panel.ProcessTitle.text = inst.name;
				panel.NextProcessTitle.text = inst.nextProcess.name[0];
				panel.NextProcessDue.text = inst.nextProcess.ConvertedDate().ToShortDateString() + " " + Regex.Replace(inst.nextProcess.ConvertedDate().ToShortTimeString(), @"(AM|PM)", "");
				panel.NextProcessStatus.text = inst.nextProcess.status;
				panel.NextProcessPriority.text = inst.nextProcess.priority;
				panel.NextProcessPerson.text = inst.nextProcess.person;
				process.transform.SetParent(WaitingContent.transform, false);
				process.SetActive(true);
			} else {
				// Instantiate failed
				process = ObjectPooler.SharedInstance.GetPooledObject(FailedPrefab);
				ErrorProcessPanel panel = process.GetComponent<ErrorProcessPanel>();
				panel.ProcessTitle.text = inst.name;
				panel.ProcessErrorMessage.text = inst.error;
				process.transform.SetParent(FailedContent.transform, false);
				process.SetActive(true);
			}
			return process;
		}).ToArray();
	}

	protected override void DepartmentClick ()
	{
		throw new System.NotImplementedException ();
	}

	protected override void DepartmentRemove ()
	{
		ObjectPooler.SharedInstance.DeactivateAll (WaitingPrefab);
		ObjectPooler.SharedInstance.DeactivateAll (FailedPrefab);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
