﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrow_rotation : DepartmentElement {
	public GameObject target;
	[SerializeField] UnityEngine.UI.Text NextRoom;

	Dictionary<string, int> directionDict = new Dictionary<string, int>();



	protected override void DepartmentUpdate(Department dep){

		var currentDirection = dep.closestRoom.direction;
		int dynamicDirection = directionDict [currentDirection];
		NextRoom.text ="Closest Room: " + dep.closestRoom.name;
		transform.rotation = Quaternion.AngleAxis(dynamicDirection, Vector3.back);

	}

	void Start(){
		directionDict.Add ("North", 0);
		directionDict.Add ("North West", 45);
		directionDict.Add ("North East", 135);
		directionDict.Add ("South", 90);
		directionDict.Add ("South West", -45);
		directionDict.Add ("South East", -135);
	}

	protected override void DepartmentRemove ()
	{
		
	}



	


	protected override void DepartmentClick(){
	}


}
