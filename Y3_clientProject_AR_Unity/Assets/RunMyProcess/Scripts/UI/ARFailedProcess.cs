﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARFailedProcess : DepartmentElement {

	public Text processTitle;
	public Text processInfo;
	public GameObject target;
	public GameObject imagetarget;

	protected override void DepartmentUpdate(Department dep){
		//this.gameObject.SetActive (true);

		//TODO: Find latest failing process in department
		//TODO: Get image for process and load in panel
		Vector2 cords = new Vector2(2,2);
		Vector2 tempCords = new Vector2 (1,2);
		processTitle.text = "CWL HR OFFBOARDING";
		processInfo.text = "STATUS: FAILED";
		float angle = GetAngle(cords, tempCords);
		Debug.Log("an angle " + angle);
		transform.RotateAround (target.transform.position, -transform.up, NegatedAngle(angle));
	}

	protected override void DepartmentRemove ()
	{
		
	}

	private float GetAngle(Vector2 department, Vector2 process){
		// https://stackoverflow.com/a/12892493
		float xDiff = process.x - department.x;
		float yDiff = process.y - department.y;
		return Mathf.Atan2 (yDiff, xDiff) * 180 / Mathf.PI;
	}
		
	private float NegatedAngle(float realAngle){
		//float negated = realAngle - transform.eulerAngles.x;
		float negated = realAngle / 2;
		Debug.Log (transform.localEulerAngles.x);
		Debug.Log (transform.eulerAngles.x);
		Debug.Log ("Negated " + negated);
		return negated;
	}

	protected override void DepartmentClick(){
	}

	// Use this for initializations
	void Start () {
		//TestRotation ();
	}

	private void TestRotation(){
		//float xDiff = 2 - 2;
		//float yDiff = 1 - 2;
		//float angle = Mathf.Atan2 (yDiff, xDiff) * 180 / Mathf.PI;

		float angle = GetAngle (new Vector2(2,2), new Vector2(2,1));

		Debug.Log ("Angle = " + Vector2.SignedAngle(new Vector2(1,1), new Vector2(2,2)));
		Debug.Log ("Angle2 = " + angle);
		Debug.Log ("Before: " + transform.position + " " + transform.eulerAngles);
		//transform.localRotation = Quaternion.Euler (angle, transform.eulerAngles.y, transform.eulerAngles.z);
		transform.RotateAround (target.transform.position, Vector3.up, angle);
		//transform.rotation = Quaternion.Euler (angle, transform.eulerAngles.y, transform.eulerAngles.z);
		//transform.Rotate(angle, 0, 0);
		Debug.Log ("After: " + transform.position + " " + transform.eulerAngles);
	}
	
	// Update is called once per frame
	void Update () {
		//transform.RotateAround (target.transform.position, -transform.up, 5.0f * Time.deltaTime);
		//Debug.Log("at update : " + target.transform.position + " " + target.transform.rotation);
	}
}

