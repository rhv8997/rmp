﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Augmentation pattern / code followed and referenced from Augmented Reality for Developers by Jonathan Linowes and 
 * Krystian Babilinski Octobet 2017 - https://www.packtpub.com/web-development/augmented-reality-developers
 * 
 * 
*/
public abstract class DepartmentElement : MonoBehaviour {
	protected abstract void DepartmentUpdate (Department dep);
	protected abstract void DepartmentClick ();
	protected abstract void DepartmentRemove ();

	/*
	 * Method is used to register event lisnters to OnDepartmentUpdate and OnDepartmentLost methods setting
	 * DepartmentUpdate and DepartmentRemove as callbacks respectively
	 *
	 * As class is abstract all scripts that inherit this class will have their implementations of DepartmentUpdate
	 * and DepartmentRemove called when a corresponding event is invoked
	*/
	void Awake(){
		FindObjectOfType<RunMyProcessCloudManager> ().OnDepartmentUpdate.AddListener (DepartmentUpdate);
		FindObjectOfType<RunMyProcessTrackableEventHandler> ().OnDepartmentLost.AddListener (DepartmentRemove);
	}
}
