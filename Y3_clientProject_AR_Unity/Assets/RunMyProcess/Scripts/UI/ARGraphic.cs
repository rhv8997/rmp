﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARGraphic : DepartmentElement {
	private GameObject currentGraphic;
	[HideInInspector]
	public Department graphicOrg;

	protected override void DepartmentUpdate(Department org){
		graphicOrg = org;
		if (currentGraphic != null) {
			Destroy (currentGraphic);
			currentGraphic = null;
		}
		if (!string.IsNullOrEmpty(org.room)){
			currentGraphic = Instantiate (Resources.Load (org.room, typeof(GameObject)), this.transform) as GameObject;
		}
	}

	protected override void DepartmentClick(){
		Debug.Log ("org clicked!");
	}

	protected override void DepartmentRemove ()
	{
		
	}

	public void testClick(){
		Debug.Log ("button clicked!");
	}

	// Use this for initialization
	void Start () {
		graphicOrg = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
