﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ARDepartmentOverview : DepartmentElement {
	public Text DepartmentName;
	public GameObject OverviewContentPanel;
	private GameObject[] processes;
	private static string Prefab = "ProcessPanelImage";
	private Func<GameObject[], Func<List<Instance>, GameObject[]>> DepopulateAndRepopulate;

	protected override void DepartmentUpdate (Department dep)
	{
		DepartmentName.text = dep.room;
		// Creates Array of GameObject panels for each process in List, deletes any existing panels that may already exist
		//processes = DepopulateAndRepopulate (processes) (dep.data);
		ObjectPooler.SharedInstance.DeactivateAll(Prefab);
		Populate(dep.data);

	}

	private GameObject[] Populate(List<Instance> processes){
		return processes.Select (proc => {
			GameObject process = ObjectPooler.SharedInstance.GetPooledObject(Prefab);
			ProcessesPanelImage panel = process.GetComponent<ProcessesPanelImage>();
			Debug.Log(panel.transform);
			panel.processNameText.text = proc.name;
			if (proc.status == "201") {
				panel.processStatusImage.color = Color.green;
			} else if (proc.status == "102") {
				panel.processStatusImage.color = Color.yellow;
			} else {
				panel.processStatusImage.color = Color.red;
			}
			panel.processTime.text = proc.ConvertedDate().ToShortDateString();
			process.transform.SetParent (OverviewContentPanel.transform, false);
			process.SetActive(true);
			return process;
		}).ToArray();
	}

	protected override void DepartmentClick ()
	{
		
	}

	protected override void DepartmentRemove ()
	{
		ObjectPooler.SharedInstance.DeactivateAll (Prefab);
	}

	// Use this for initialization
	void Start () {
		// Using currying for extendability
		DepopulateAndRepopulate = panels => processList => {
			//if (panels != null || panels.Length > -1){
			//	foreach (GameObject process in panels) {
			//		Destroy (process);
			//	}
			//}
			return Populate(processList);
		};
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
