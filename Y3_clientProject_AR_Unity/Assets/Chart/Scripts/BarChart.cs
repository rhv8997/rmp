﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;

public class BarChart : MonoBehaviour {
	private GameObject[] bars;
	private float min;
	private float max;
	private float med;
	private float difference;
	private List<BarData> barData;
	private float x_length = -0.1115484f;
	private bool containsZero;

	public Text chartTitle;
	public Text MinText;
	public Text MedText;
	public Text MaxText;
	public GameObject x;
	public GameObject y;
	public GameObject X_Min;
	public GameObject X_Med;
	public GameObject X_Max;

	public class BarData {
		public string name;
		public float value;
		public string groupValue;
		public Color colour;

		public BarData(string dataName, float dataValue) {
			name = dataName;
			value = dataValue;
		}

		public BarData(string dataName, float dataValue, Color barColour) {
			name = dataName;
			value = dataValue;
			colour = barColour;
		}

		public BarData(string dataName, float dataValue, string groupByVal, Color barColour){
			name = dataName;
			value = dataValue;
			groupValue = groupByVal;
			colour = barColour;
		}
	}

	public void plot<C, G>(string title, BarData[] data, ChartOptions<C, G> options){
		
	}

	public void plot(string title, List<BarData> data, bool toGroup, bool toOrder){
		if (toGroup) {
			barData = normalise (groupBy (data));
		} else {
			barData = normalise (data);
		}
		if (toOrder) {
			barData = barData.OrderBy (bar => bar.value).ToList();
		}
		x.transform.localScale = new Vector3 (0.0f, 0.0f, x.transform.localScale.z * barData.Count);
		initialise ();
		chartTitle.text = title;
		MinText.text = "" + 0;
		MaxText.text = "" + max;
		MedText.text = "" + med;
	}

	private void initialise() {
		bars = new GameObject[barData.Count];
		for (int i = 0; i < bars.Length; i++) {
			GameObject currentBar = Instantiate (Resources.Load("BarParent", typeof(GameObject)), this.transform) as GameObject;
			currentBar.transform.localPosition = new Vector3 (currentBar.transform.localPosition.x,
				currentBar.transform.localPosition.y, i * x_length);
			BarParent parent = currentBar.GetComponent<BarParent> ();
			float yScale = containsZero && barData [i].value == 0 ? 0.0f : barData [i].value + 0.1f;
			parent.bar.transform.localScale = new Vector3 (1.0f, yScale, 1.0f);
			parent.barCube.GetComponent<Renderer>().material.color = barData [i].colour;
			parent.nameText.text = barData [i].name;
			bars [i] = currentBar;
		}
	}

	// reference:
	// https://docs.microsoft.com/en-us/dotnet/api/system.linq.enumerable.groupby?view=netframework-4.7.2
	private List<BarData> groupBy(List<BarData> toGroup){
		var query = toGroup.GroupBy (bar => bar.groupValue, bar => bar.groupValue, (val, vals) => new {
			Key = val,
			Count = vals.Count ()
		});
		return query.Select (barGroup => new BarData(barGroup.Key, (float) barGroup.Count, toGroup.Find(obj => obj.groupValue == barGroup.Key).colour)).ToList();
	}

	public void click(){
		//Material mat = GetComponent<Renderer>().material;
		//mat.color = Color.red;
		MaxText.text = "CLICKED";
		Renderer[] renders = y.GetComponentsInChildren<Renderer> ();
		foreach (Renderer render in renders) {
			render.material.color = Color.red;
		}
	}

	private List<BarData> normalise(List<BarData> toNormalise){
		float[] vals = toNormalise.Select (d => d.value).ToArray();
		min = vals.Min ();
		max = vals.Max ();
		med = median (vals);
		if (min == 0) {
			containsZero = true;
		}
		difference = max - min;
		foreach (BarData d in toNormalise) {
			d.value = (d.value - min) / difference;
		}
		return toNormalise;
	}

	// reference:
	// https://blogs.msmvps.com/deborahk/linq-mean-median-and-mode/
	private float median(float[] data){
		int count = data.Count ();
		int middle = count / 2;
		var sorted = data.OrderBy (n => n);
		float median = 0f;
		if ((count % 2) == 0) {
			median = ((sorted.ElementAt (middle) + sorted.ElementAt ((middle - 1))) / 2);
		} else {
			median = sorted.ElementAt (middle);
		}
		return median;
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
