﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ChartOptions<C, G> {
	public bool toGroup;
	public bool toOrder;
	public bool includeTitle;
	public bool includeNames;
	public Func<C, C, int> comparison;
	public G groupBy;

	public class ChartComparison<T>{
		public Func<T, T, int> comparison;
	}

	public class GroupBy<T>{
		public T groupBy;
	}

	public ChartOptions(bool grouped, bool ordered, bool title, bool names, Func<C, C, int> compared, G groupedWith){
		toGroup = grouped;
		toOrder = ordered;
		includeTitle = title;
		includeNames = names;
		comparison = compared;
		groupBy = groupedWith;
	}
}
