﻿using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class SimpleCloudHandler : MonoBehaviour, ICloudRecoEventHandler
{
	public ImageTargetBehaviour ImageTargetTemplate;
    private CloudRecoBehaviour mCloudRecoBehaviour;
	ObjectTracker m_ObjectTracker;
	TrackableSettings m_TrackableSettings;
	RunMyProcessCloudManager m_CloudRecoContentManager;
    private bool mIsScanning = false;
    GUIStyle gustyle;
    // Use this for initialization
    void Start()
    {
        // register this event handler at the cloud reco behaviour
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
		m_TrackableSettings = FindObjectOfType<TrackableSettings>();
		m_CloudRecoContentManager = FindObjectOfType<RunMyProcessCloudManager>();

        if (mCloudRecoBehaviour)
        {
            mCloudRecoBehaviour.RegisterEventHandler(this);
        }
    }

    public void OnInitialized()
    {
        Debug.Log("Cloud Reco initialized");
		// get a reference to the Object Tracker, remember it
		m_ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
    }

    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Cloud Reco init error " + initError.ToString());
    }

    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Cloud Reco update error " + updateError.ToString());
    }

    public void OnStateChanged(bool scanning)
    {
        mIsScanning = scanning;
		if (scanning)
		{
			// clear all known trackables
			m_ObjectTracker.TargetFinder.ClearTrackables(false);

		}
    }

    // Here we handle a cloud target recognition event
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
		m_CloudRecoContentManager.HandleTargetFinderResult(targetSearchResult);
        // do something with the target metadata
		m_ObjectTracker.TargetFinder.ClearTrackables(false);

		if (ImageTargetTemplate) {
			ImageTargetBehaviour imageTargetBehaviour =
				(ImageTargetBehaviour)m_ObjectTracker.TargetFinder.EnableTracking(
					targetSearchResult, ImageTargetTemplate.gameObject) as ImageTargetBehaviour;
			
			if (m_TrackableSettings && m_TrackableSettings.IsExtendedTrackingEnabled())
			{
				imageTargetBehaviour.ImageTarget.StartExtendedTracking();
			}
		}
        // stop the target finder (i.e. stop scanning the cloud)
        mCloudRecoBehaviour.CloudRecoEnabled = false;
    }

    void OnGUI()
	{
	}
}
