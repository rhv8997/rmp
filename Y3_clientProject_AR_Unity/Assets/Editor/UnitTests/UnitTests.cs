﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class NewPlayModeTest {
	Department department = null;

	[SetUp]
	public void ARDepartmentTestsSetUp(){
		department = new Department ();
		department.room = "Test Department";
		department.id = "1234567890";
		department.data = new List<Instance> ();
		department.employees = new List<Employee> ();

		ClosestRoom room = new ClosestRoom ();
		room.name = "Test";
		room.direction = "North West";
		department.closestRoom = room;

		Employee emp1 = new Employee ();
		emp1.id = 1;
		emp1.name = "Test test";
		emp1.active = true;
		Employee emp2 = new Employee ();
		emp2.id = 2;
		emp2.name = "Test Two";
		emp2.active = false;
		department.employees.Add (emp1);
		department.employees.Add (emp2);

		Instance completedInstance = new Instance ();
		completedInstance.name = "Test Instance 1";
		completedInstance.status = "201";
		completedInstance.published = DateTime.Now.ToString();
		Instance failedInstance = new Instance ();
		failedInstance.name = "Test Instance 2";
		failedInstance.status = "301";
		failedInstance.published = DateTime.Now.ToString();
		failedInstance.person = "Test tes";
		failedInstance.error = "Invalid host reference";
		Instance waitingInstance = new Instance ();
		waitingInstance.name = "Test Instance 3";
		waitingInstance.status = "102";
		waitingInstance.published = DateTime.Now.ToString();
		NextProcess nextProcess = new NextProcess ();
		nextProcess.name = new string[]{"Test Next Process"};
		nextProcess.due = DateTime.Now.ToString();
		nextProcess.person = "Test test";
		nextProcess.priority = "MEDIUM";
		nextProcess.status = "OPENED";
		waitingInstance.nextProcess = nextProcess;
		Instance completedInstance2 = new Instance ();
		completedInstance2.name = "Test Instance 4";
		completedInstance2.status = "201";
		completedInstance2.published = DateTime.Now.ToString();

		department.data.Add (completedInstance);
		department.data.Add (failedInstance);
		department.data.Add (waitingInstance);
		department.data.Add (completedInstance2);


	}

	[Test]
	public void NewPlayModeTestSimplePasses() {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator NewPlayModeTestWithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}

	[UnityTest]
	public IEnumerator DepartmentHealthBar(){
		GameObject healthBar = GameObject.Instantiate(Resources.Load("Slider3D", typeof(GameObject))) as GameObject;
		HealthUI ui = healthBar.GetComponent<HealthUI> ();
		ui.healthValue(department.ForLoop());
		Assert.AreEqual (new Vector3 (department.ForLoop (), 1.0f, 1.0f), ui.innnerHealth.transform.localScale);
		yield return null;
	}

	[UnityTest]
	public IEnumerator DepartmentBarChart(){
		GameObject chart = GameObject.Instantiate (Resources.Load ("Chart", typeof(GameObject))) as GameObject;
		BarChart barChart = chart.GetComponent<BarChart> ();
		barChart.plot ("Processes", department.data.Select (inst => {
			Color colour;
			if (inst.status == "201") {
				colour = Color.green;
			} else if (inst.status == "102") {
				colour = Color.yellow;
			} else {
				colour = Color.red;
			}
			return new BarChart.BarData (inst.name, 0f, inst.status, colour);
		}).ToList (), true, false);
		int barCount = chart.GetComponentsInChildren<BarParent> ().Length;
		Assert.AreEqual (3, barCount);
		yield return null;
	}

	[UnityTest]
	public IEnumerator AREmployeesDepartmentUpdate(){
		GameObject canvas = new GameObject ();
		canvas.AddComponent<Canvas> ();
		yield return null;
	}
}
