﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class ARDepartmentTests {
	Department department = null;

	[SetUp]
	public void ARDepartmentTestsSetUp(){
		department = new Department ();
		department.room = "Test Department";
		department.id = "1234567890";
		department.data = new List<Instance> ();
		department.employees = new List<Employee> ();

		ClosestRoom room = new ClosestRoom ();
		room.name = "Test";
		room.direction = "North West";
		department.closestRoom = room;

		Employee emp1 = new Employee ();
		emp1.id = 1;
		emp1.name = "Test test";
		emp1.active = true;
		Employee emp2 = new Employee ();
		emp2.id = 2;
		emp2.name = "Test Two";
		emp2.active = false;
		department.employees.Add (emp1);
		department.employees.Add (emp2);

		Instance completedInstance = new Instance ();
		completedInstance.name = "Test Instance 1";
		completedInstance.status = "201";
		completedInstance.published = DateTime.Now.ToString();
		Instance failedInstance = new Instance ();
		failedInstance.name = "Test Instance 2";
		failedInstance.status = "301";
		failedInstance.published = DateTime.Now.ToString();
		failedInstance.person = "Test tes";
		failedInstance.error = "Invalid host reference";
		Instance waitingInstance = new Instance ();
		waitingInstance.name = "Test Instance 3";
		waitingInstance.status = "102";
		waitingInstance.published = DateTime.Now.ToString();
		NextProcess nextProcess = new NextProcess ();
		nextProcess.name = new string[]{"Test Next Process"};
		nextProcess.due = DateTime.Now.ToString();
		nextProcess.person = "Test test";
		nextProcess.priority = "MEDIUM";
		nextProcess.status = "OPENED";
		waitingInstance.nextProcess = nextProcess;
		Instance completedInstance2 = new Instance ();
		completedInstance2.name = "Test Instance 4";
		completedInstance2.status = "201";
		completedInstance2.published = DateTime.Now.ToString();

		department.data.Add (completedInstance);
		department.data.Add (failedInstance);
		department.data.Add (waitingInstance);
		department.data.Add (completedInstance2);


	}

	[Test]
	public void ARDepartmentTestsSimplePass() {
		// Use the Assert class to test conditions.
		bool isActive = false;
		Assert.AreEqual (false, isActive);
	}

	[Test]
	public void DepartmentCalculateHealth(){
		Assert.AreEqual (0.5f, department.ForLoop());
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator ARDepartmentTestsWithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}
}
