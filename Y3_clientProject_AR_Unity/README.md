# Unity AR app

## Opening the Project in Unity (version 2017.4.10f1)

1. Clone or download the repository
2. Launch Unity and click **Open** located at the top right - the version of Unity must be version 2017.4.10f1
3. Navgiate to the cloned repository, locate and select the VRProject.scene file found under **unity-test/Assets/RunMyProcess/Scenes**
4. If you cannot find the scenes then load the project based on the unity-test folder and when it loads in unity, in the assets panel go to Assets/RunMyProcess/Scenes and select the VRProject.scene file

## Running the Application in Unity
1. Load the VRProject scene
2. Click the **Play** button located at the top center

## Building and Deploying to an Android Device
1. Navigate to the **Build Settings** screen **File > Build Settings** (Ctrl-Shift-B)
2. Select Android from the platform list and click the **Swith Platform** located below the Platform's list
3. Select only the **VRProject** scene in the **Scenes In Build** section located at the top of the screen
4. Navigate to **Player Settings** located at the bottom right
5. Under **Other Settings** tab ensure that the **Identifcation** section is filled out accordingly to ensure that the application has a Package Name, Version, Bundle Code and a Minimum and Target API Level that matches that of the target Android device being used.
6. Click the **Build And Run** button (Ctrl-B)
7. Save the APK with a relevant name
8. The application will automatically launch on the target device

## Running Unit Tests
1. Within Unity navigate to the **Test Runner** screen **Window > Test Runner**
2. Switch to **Edit Mode**
3. Click **Run All**
4. Switch to **Play Mode**
5. Click **Run All**

There are very few unit tests, all of which have not been written with a TDD approach, this is in part due to there appearing to be a very limited number of learning resources available for Unit testing in Unity. This is reflected by Unity's official documentation on [unit testing](https://docs.unity3d.com/Manual/PlaymodeTestFramework.html) being very brief and minimal as well as their blog posts for [unit testing](https://blogs.unity3d.com/2014/05/21/unit-testing-part-1-unit-tests-by-the-book/) being a few versions out-of-date.

Unity 2017.4 comes with NUnit built-in this allows all normal Plain Old C# scripts to be unit tested as normal. These types of unit tests can be tested within **Edit Mode** as they don't require any Unity context. Unit tests that are too be tested in **Edit Mode** must be within a C# script under the **unity-test/Assets/Editor** directory. Only one such file exists currently and is called **unity-test/Assets/Editor/ARDepartmentTests.cs**

Unit tests can be written to test Unity GameObject's however these are more complex and have little in the way of learning resources that document correct usage. Unit tests that will test more complex use cases and GameObject's are tested in **Play Mode** as they require Unity contexts in order to safely Instantiate and Destroy GameObject's. They can be placed in any C# script under **unity-test/Assets/Editor/UnitTests**. Only one such file exists currently and is called **unity-test/Assets/Editor/UnitTests/UnitTests.cs**

Due to the design of the application, requiring Vuforia SDK, UnityEvent's and a UnityWebRequest which would all need to be mocked using an external framework such as NSubstitute in order to unit test as well as the project's small time frame, we decided not to explore any further into mocking within Unity, and thus a large majority of the source code that requires Unity contexts doesn't have test cases.

Resources for unit testing:
* https://www.dima.to/blog/?p=499
* https://cantina.co/writing-unit-tests-in-unity-part-1/

## Third Party Libraries

### Vuforia
Vuforia is a third-party library for creating augmented reality software for Android devices. The library is used for image target recognition, in which this application utilises their Cloud database technology allowing a potential of over 1 million image targets. The library is also used for the creation and projection of augmented 3D objects additionally allowing for extended tracking of rendered augmented objects.

We chose to use Vuforia SDK over other Android AR SDKs because it has more advanced features such as **Extended Tracking** and **Cloud Image** Targets which Vuforia provide a cloud database to store Image Targets along with metadata which is necessary for the project.
This was also why we chose Unity over other engines because it has built-in support for Vuforia.

Please note that Vuforia’s Cloud technology is a freemium model and only allows for 1000 free image recognitions a month.

Learn how to create your own Vuforia database and link it with Unity [here](https://docs.unity3d.com/Manual/vuforia_get_started_image_targets_setup.html)

## Architecture

### Vuforia Cloud Recognition

The application uses Vuforia Cloud for the recognition of image targets, below is a brief outline of the necessary prefabs/scripts needed.

* Prefab Name: **CloudRecognition** – Use this prefab to add your Vuforia Cloud database API keys, within the inspector.
    * Script: **SimpleCloudHandler** – A custom implementation of Vuforia’s **CloudRecoEventHandler**. The most important method in this class is the **OnNewSearchResult** which gets called when a new image is recognised. The method then calls the **HandleTargetFinderResult** of the **RunMyProcessCloudManager** script.
* Prefab Name: **CloudManager** – Responsible for delegating the recognition event to the necessary scripts.
    * Script: **RunMyProcessCloudManager** – A custom implementation of Vuforia’s **CloudRecoContentManager**. The **HandleTargetFinderResult** is called with the newly recognised target. This method extracts the meta data which contains the ID of the Department from the recognised target and then makes an API call to the backend server passing the ID as a param. The returned JSON is then de-serialised into C# classes.
* Prefab Name: **ImageTarget** – This represents the recognised image and is responsible for the AR content.
    * Script: **ImageTargetBehaviour** – Vuforia script for the recognised image target
        * Options – The type is set to Cloud Reco, Extended Tracking is enabled
    * Script: **RunMyProcessTrackableEvent** – A custom implementation of Vuforia’s **CloudRecoTrackableEvent**. The script defines a DepartmentLost event which is invoked in the **OnTrackingLost** method of the class. The event will inform any scripts listening to the event that the target has been lost and to handle this appropriately.
    * Script: **ObjectPooler** – An object pooler used for the UI panels which will be populated by the data returned from the API call. A pooler is used to improve performance upon recognition of a target.
* Prefab Name: **ARCamera** – This represents the actual device camera.
    * Script: **VuforiaBehaviour** – Vuforia script for configuring the camera used by the application.
        * Options – World Center Mode set to _DEVICE_TRACKING_, sets the camera to the world origin at runtime (0,0,0)

We chose to use Vuforia's Cloud Image Target's over Vuforia Image Target's because this allows us to add the department ID as metadata to targets. Additionally, Cloud Target's make the application far more scalable because over a million image targets can be added to the cloud database. Furthermore, new targets can be added without requiring the APK to be rebuilt unlike Vuforia Image Target's.

#### Reference
* [Vuforia Cloud Target](https://library.vuforia.com/articles/Training/Cloud-Recognition-Guide)

### Fetching Data via the Server API
When an image is recognised an API call is made to the server in a separate thread to avoid blocking the main thread which can cause visible lag to the user. Given the requested department ID from the target's metadata is valid the server will return a JSON object containing data about the department such as the employees, instances, closest department, etc.

```csharp
StartCoroutine (RequestDepartment(url, targetSearchResult.MetaData.Trim('"')));
```

``` csharp
IEnumerator RequestDepartment(string uri, string id){
		UnityWebRequest request = UnityWebRequest.Get (uri);
		yield return request.SendWebRequest ();
		if (request.isNetworkError || request.isHttpError) {
			Debug.Log (request.downloadHandler.text);
		} else {
			Debug.Log (request.downloadHandler.text);
			var dep = Department.CreateFromJSON (request.downloadHandler.text);
			dep.id = id;
			OnDepartmentUpdate.Invoke (dep);
		}
	}

```

The returned JSON is de-serialised into C# classes using the **JSONUtility** library which is included by default with latest versions of Unity.

Below is the structure of the C# classes:

* Department.cs
``` csharp
[System.Serializable]
public class Department {
	public string id;
	public List<Employee> employees;
	public string room;
	public List<Instance> data;
	public ClosestRoom closestRoom;
}
```
* Employee.cs
``` csharp
[System.Serializable]
public class Employee {
	public string name;
	public bool active;
	public int id;
}
```
* Instance.cs
``` csharp
[System.Serializable]
public class Instance {
	public string id;
	public string name;
	public string status;
	public string published;
	public NextProcess nextProcess;
	public string error;
	public string person;
}
```
* ClosestRoom.cs
``` csharp
[System.Serializable]
public class ClosestRoom {
	public string name;
	public string direction;
}
```
We chose to use JSONUtility as the JSON de-serializer because it's included by default with Unity thus minimizing the need for further third party libraries. Additionally, the library is very easy to integrate and use. However, it does mean that de-serialised C# objects don't follow the SOLID principles of OOP because its class members must be public in order for the library to work as intended.

#### Reference
* [Parsing JSON with C#](https://medium.com/@MissAmaraKay/parsing-json-in-c-unity-573d1e339b6f)
* [Using UnityWebRequest](https://stackoverflow.com/questions/46003824/sending-http-requests-in-c-sharp-with-unity/46008025#46008025)


### Department Events

The creation and deletion of augmented objects are handled through **UnityEvent**'s. There are two events which occur during the application lifecycle.

* **DepartmentEvent** (OnDepartmentUpdate) - This event is defined within the **RunMyProcessCloudManager** and is invoked when an image has been recognised and the department data has been loaded from the server API.

``` csharp
public class DepartmentEvent : UnityEvent<Department> {} // declaration
public DepartmentEvent OnDepartmentUpdate = new DepartmentEvent (); // instantiate instance
IEnumerator RequestDepartment(string uri, string id){
		...
    OnDepartmentUpdate.Invoke (Department department); // invoke with Department instance
    ...
	}

```

* **DepartmentLost** (OnDepartmentLost) - This event is defined within the **RunMyProcessTrackableEventHandler** and is invoked when tracking of an image or augmented object is lost by Vuforia.

``` csharp
public class DepartmentLost : UnityEvent {} // declaration
public DepartmentLost OnDepartmentLost = new DepartmentLost(); // instantiate instance
OnDepartmentLost.Invoke (); // invoke
```

### DepartmentElement

**DepartmentElement** is an abstract class that inherits from **MonoBehaviour** thus classes inheriting **DepartmentElement** don't need to also inherit from **MonoBehaviour** because they will inherit the methods from DepartmentElement. DepartmentElement has two abstract methods called **DepartmentUpdate** and **DepartmentRemove** these two methods are added as callbacks to the **OnDepartmentUpdate** and **OnDepartmentLost** UnityEvent's respectively within it's **Awake** method.

``` csharp
void Awake(){
		FindObjectOfType<RunMyProcessCloudManager> ().OnDepartmentUpdate.AddListener (DepartmentUpdate);
		FindObjectOfType<RunMyProcessTrackableEventHandler> ().OnDepartmentLost.AddListener (DepartmentRemove);
}
```

* DepartmentUpdate - Takes a Department object as an argument to recieve the Department passed by the invoking UnityEvent
``` csharp
protected abstract void DepartmentUpdate (Department dep);
```
* DepartmentRemove - Takes no arguments as the invoking event doesn't send any data
``` csharp
protected abstract void DepartmentRemove ();
```

This design follows the **Event Observer Pattern** outlined in Augmented Reality for Developers publication. The benefit of the pattern is that the application is far more scalable, testable and refactorable as all augmented objects are given autonomy.
#### Reference
* [Augmented Reality for Developers By Jonathan Linowes, Krystian Babilinsk, October 2017, Chapter 7](https://www.packtpub.com/web-development/augmented-reality-developers)

### Augmented Objects

#### Static Augmented Objects
Augmented objects that don't rely on the data returned from the server can simply be added as a child of the **ImageTarget**.
#### Dynamic Augmented Objects
Augmented objects that rely on the data returned from the server will need to have their own C# scripts which will be responsible for the creation of the object.
The dynamic augmented object script must inherit from **DepartmentElement** and have non-abstract implementations for the **DepartmentUpdate** and **DepartmentRemove** methods. As the script inherits from **DepartmentElement** the class will add an event listener onto the relevant UnityEvent's on application start. Thus the method implementations in the augmented object's script will be called when the corresponding event is invoked. Within the augmented object's script **Start** method instantiate the required Prefab ensuring to save as a private member of the class. An empty GameObject should be added to the **ImageTarget** as a child and have the corresponding script added as a Component.

* Example augmented object script
``` csharp
public class ARDepartmentOverview : DepartmentElement {
	public Text DepartmentName;
	public GameObject OverviewContentPanel;
	private GameObject[] processes;
	private static string Prefab = "ProcessPanelImage";

  // handle new Department object received from server
	protected override void DepartmentUpdate (Department dep)
	{
		DepartmentName.text = dep.room; // access data from dep
		ObjectPooler.SharedInstance.DeactivateAll(Prefab);
		Populate(dep.data);
	}

  // handle removing Department
	protected override void DepartmentRemove ()
	{
		ObjectPooler.SharedInstance.DeactivateAll (Prefab);
	}

  // Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

}

```

* Note: Prefab's can be added to the GameObject within the editor thus the Prefab doesn't need to be instantiated within the augmented object's script **Start** method.

### Object Pooling
The **DepartmentOverview**, **EmployeesOverview** and **Failed/WaitingOverivew** Canvas' are where the dynamic UI elements will be added when the data is retrieved from the server. For each process or employee added to the Canvas a Panel prefab must be instantiated and added as a child. Additionally, if there are any existing loaded Panel Prefabs then they must be destroyed first. The instantiation and destruction of GameObject's at runtime can be very taxing on the CPU.
When testing the application we found that for some department's there were more than 15 processes being loaded from the server at once. This had a detrimental effect on the application performance causing noticeable lag. For this reason we created an Object Pooling script that will instantiate a specified number of any number of prefabs for use throughout the application upon application start (inside the **Start** method) instead of at runtime. Additionally, the Object Pooling script allows for the expansion of the object pool thus allowing for scalability in the future if more prefabs must be loaded in.

#### Reference
* [Object Pooling in Unity](https://www.raywenderlich.com/847-object-pooling-in-unity)

### Augmented Bar Chart
During development we worked on and developed a Unity prefab called **Chart.prefab** that can be used to display numeric data in a 3D bar chart.

#### Usage
Data can be plotted using the public method `plot` of the **BarChart.cs** Component of the **Chart.prefab**, and takes the following parameters
* `string title` - Name for the chart as a string.
* `List<BarData> data` - The actual data in BarData format.
* `bool toGroup` - Boolean specifying whether data should be grouped based on `groupValue` within **BarData** object.
* `bool toOrder` - Boolean specifying whether the data should be ordered in ascending order.

**BarData** is a subclass of **BarChart.cs** and holds data about an individual bar within the chart.
``` csharp
public class BarData {
		public string name; // name of the bar
		public float value; // float value of the bar
		public string groupValue; // string value used to group multiple bars into one
		public Color colour; // desired material colour of the bar
}
```
* Plotting data from scripts
``` csharp
currentGraphic.GetComponent<BarChart> ().plot ("Processes",
				dep.data.Select(inst => { // map List of objects to List of BarData objects
					Color colour;
					if (inst.status == "201"){
						colour = Color.green;
					} else if (inst.status == "102"){
						colour = Color.yellow;
					} else {
						colour = Color.red;
					}
					return new BarChart.BarData(inst.name, 0f, inst.status, colour);
				}).ToList(), true, false);
```
