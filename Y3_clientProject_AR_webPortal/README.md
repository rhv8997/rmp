![alt text](http://viewmyprocess.co.uk/static/media/logo.f24fe3fb.JPG "RunMyProcess")

# ViewMyProcess Web Portal

###### A web application built for RunMyProcess, Fujitsu. 

***
ViewMyProcess is a system that offers managers a unique way of viewing the status of departments in a business. It acheives this through a modern Augmented Reality (AR) UI application. This application is powered by this React-Node portal.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. There are also further instructions on how to deploy the web portal to Amazon Web Services and load the webpage and API's.

The application is running on an AWS instance with IP ec2-18-130-43-56.eu-west-2.compute.amazonaws.com on the domain viewmyprocess.co.uk.

To access the API, you can go to `http://viewmyprocess.co.uk:4000/api/rooms?id=9d6c698ce9391544782394895` and change the ID to any from the database.
We recomend downloading a [chrome JSON viewer](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh) to read the JSON easier

### Prerequisites

What you need to install and how to install them

To download the app you are going to need a git client of your choice. Some good ones are:

- Git Bash - https://gitforwindows.org/
- GitHub Desktop - https://desktop.github.com/

You are also going to need node installed on your system to run the node server. Node can be installed from [here]( https://nodejs.org/en/download/).

### Installing and running
The application when cloned is production ready. I.e It makes requests to our AWS servers. 

Clone the project using the following command:

```
SSH:
git clone git@gitlab.cs.cf.ac.uk:c1673107/react-project-group2.git
```
```
HTTPS:
git clone https://gitlab.cs.cf.ac.uk/c1673107/react-project-group2.git
```

Now that the project is on the system, you can choose to open it with an IDE terminal or just run it via the OS terminal. We recommend to use the [WebStorm](https://www.jetbrains.com/webstorm/) IDE terminal.

Once you have a terminal open, navigate to the root of the project 'react-project-group2' and run:

```
npm install
```

This is to install all the packages listed in the later section which are in the package.json file. Once npm has finished, run the command:

```
npm start
```

This will start the project on localhost port 3000. The server also has to be started to supply the react app with data. To do this, navigate to the server directory and in a seperate terminal run:

```
cd src/server
```

and run the command:

```
node server
```

Now head to http://localhost:3000 and you will see the portal and a loading image. Click on one of the buildings which have loaded on the left nav bar. This will show you a table of rooms associated with that building. You can also click the add button to add a room to which you can associate an image, processes and employees to.

To be able to post an image to Vuforia, the SpringBoot server also needs to be up and running. Instructions on how to do that can be found [here](https://gitlab.cs.cf.ac.uk/c1673107/spring-server/tree/master):

If you want to fully start the project locally you need to change some code so that the post forms and sockets link locally, not  AWS.

Form post - src/components/form:

`const BASE_URL = 'http://localhost:4000/';`

Sockets - src/api.js:

`const socket = openSocket('http://localhost:4000/';)`

Please note however that if you run 100% locally the app on the phone will not be able to connect to these servers.

#### Tests

We did tests! - where's our medal?

In all honesty testing was difficult. With only 4 weeks to get a product to our client they specified that the main objective was the prototype, authentication and testing just wasn’t a priory. But where we had time we wrote tests. As we had split our code into components testing was much easier and we took use of the framework Jest.

We used Jest mainly because we had bootstrapped the react project with create-react-app and it comes out the box with it. However there are certainly good reasons to use it. Firstly it has a really nice interface in the command prompt making it easy to see where and why there are problems. As well as this it is generally easier than alternatives like Mocha and Chai, the snapshotting features are much quicker to write. Jest also has a watch feature (--watch) which means the tests can run parallel and automatically run when changes to the code are made. Useful!

We used Enzyme (made by AirBnb) on top of Jest as Enzyme has some great functionality, allowing us to test loads of activity and interaction such as props, button clicks and callbacks!

Jest with React makes it easy to run our tests, nothing extra needs to be installed.

Simply run the command in another terminal:

```
npm test 
```

Tests will not run straight away with Jest if none have been added since the last commit. If this is the case, the message below will show in the terminal:

```
No tests found related to files changed since last commit.
Press `a` to run all tests, or run Jest with `--watchAll`.
```

##### Simply press a on your keyboard to run all the tests.

This will run all the tests. We recommend you doing this always before running the project.

We have snapshot tests for all of our components which help us keep track of changes. If changes are made our snapshots need updating. Within these tests we also check how components are loaded and check how props change the loading. 

If you make changes to the code it is simple to update the snapshots, simply run the npm test command above and follow the instructions when the tests have ran. 

```
Typing u in the terminal when asked updates the snapshots
```

We also did some unit tests to test logic such as our coordinate script.

## Tools used

### Why did we use React?

We used React because we needed a simple one page website and the ability to Bootstrap the project with Create React App would get us going quickly.

React allowed us to work with the DOM like never before. Unlike any previous work our team had done we were able to integrate our HTML code with our JavaScript. React handles updates brilliantly and continuously brings updates to UI (reactive to changes).

The process of tree reconciliation is also interesting as it handles memory really well. It doesn't rewrite HTML everytime and uses comparison; updating the parts that are needed.

Alternatives would have included Java frameworks such as Spring so we could have the Vuforia API and portal on the same server. But this would have just been overkill for a simple one page portal such as ours.


### Bootstrapping the React Portal

We bootstrapped the application using the Create react App tool provided by React. We did this because, well, it is made by the geniuses of React so it must be good.

In all seriousness, we had a very limited time frame of 4 weeks and didn’t really care much about the configuration and build process. We needed to just get going and bootstrapping did this for us. All the Babel and Webpack nonsense was abstracted away, we have a really simple package.json file and didn’t have to write lots of boilerplate code and configuration to get going.

As stated on the website, ‘Create React App is a comfortable environment for learning React, and is the best way to start building a new single-page application in React’. For us as learners it certainly did make it easy. Alternatives such as Next.js are worth considering now we have experience with React but as our website didn’t need routing and static pages so it wouldn’t have been a good choice anyway.


### The server, what is it and why is it needed


To anybody reading, please have our sincerest apologies for the sheer amount of passion that this explanation brings, we like the Express server, and we like it a lot.

Where was the need for an Express server in this application? After all the calls to the RunMyProcess API’s could have been made via Ajax in the React portal or via several async libraries within Unity in the app. So why on earth did we create ANOTHER server? This design choice was made upon the very well known architecture design principle of the separation of concerns. A quick read through Wikipedia can explain this principle quite perfectly: ‘separating a computer program into distinct sections, such that each section addresses a separate concern’. Thank’s Wikipedia. The RunMyProcess API’s provide the data that this application runs on and without this data it is entirely pointless. The data displayed on the AR app, the departments that can be created on the portal and even the processes that one can attach comes from this API data. It would be fair to say therefore that it is the most important aspect of the application and serving the data must be a priority.

The API’s follow the model of SOAP, Simple Object Access Protocol, are XML and contain a sheer amazing amount of embedded URLs in which to navigate to. As a team we were foolish enough to commit to a design stack before fully undertaking enough research. Learn from our mistakes. We thought, ‘let's make our server, serve some dummy data, get our UI in place and at the end of week 2 we will actually start using the clients API’s’. In all fairness, we didn’t receive documentation for the API’s until the next week so we kind of had to hold off and use dummy data anyway. If we had done the research however we would have learnt that the API’s returned XML and realised that NodeJs and Express do not support that. Problem. We had to use external libraries to constantly convert the XML into JSON, an extra step which just slows the performance. This may seem like a load of waffle but the point is a good one.

What if RunMyProcess needs to change their API’s, what if they are no longer in the format of SOAP and are instead RESTful and return JSON? That is a big change in the way that we work with the data as we no longer need to convert and we did that a lot! We would have to go to all the different servers and classes such as the Unity app, the Spring server and the React portal and change the way we make the requests. That’s not good design. 

The separation of concerns ensures that ‘individual sections can be reused, as well as developed and updated independently’. If we need to change the API’s we know exactly where to go, one place, the Express server! It is the middleman of the application, it serves everything; it’s only function and it does it well. The API’s return hundreds of instances for different departments and different processes, trawling through these is chaos but it does it quick. 


### Why we used Node and Express

The biggest thing people talk about when advertising Node is its non-blocking I/O model and this would be very important to us. Here is an example of the data that is returned from one of many RunMyProcess API’s:

```
<feed xmlns="http://www.w3.org/2005/Atom" xml:base="https://live.runmyprocess.com/">
<link rel="self" type="application/atom+xml" href="config/112761542179152739/user/993777/project/?filter=NAME&operator=CONTAINS&value=CWL%24_%24eng&method=GET&P_rand=17306"/>
<link rel="customer" href="customer/112761542179152739"/>
<id/>
<generator>(c) RunMyProcess 2018</generator>
<rights>[]</rights>
<updated>2019-01-10T11:49:42Z</updated>
<entry>
<title>CWL Eng Test</title>
<link rel="version" href="config/112761542179152739/project/215358/version/"/>
<link rel="process_as_service" href="config/112761542179152739/service?filter=TYPE+PROJECT&operator=EE+EE&value=PRO+215358"/>
<link rel="stat" href="config/112761542179152739/project/215358/stat/"/>
<link rel="widget_appli" href="config/112761542179152739/appli?filter=SUBTYPE+PROJECT&operator=IN+EE&value=WIDGET+215358"/>
<link rel="appli" href="config/112761542179152739/appli?filter=PROJECT&operator=EE&value=215358"/>
<link rel="process" href="config/112761542179152739/process?filter=PROJECT&operator=EE&value=215358"/>
<link rel="host" href="config/112761542179152739/host?filter=PROJECT&operator=EE&value=215358"/>
<link rel="customlist" href="live/112761542179152739/data?filter=PROJECT&operator=EE&value=215358"/>
<link rel="uploaded_images" href="live/112761542179152739/project/215358/upload?filter=TYPE&operator=EE&value=image"/>
<link rel="versioned_images" href="config/112761542179152739/project/215358/file/?filter=TYPE&operator=EE&value=image"/>
<link rel="file_by_extension" href="live/112761542179152739/project/215358/upload?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="versionedfile_by_extension" href="config/112761542179152739/project/215358/file/?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="service" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215358+EXT"/>
<link rel="api" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215358+COMP"/>
<link rel="processquery" href="config/112761542179152739/project/215358/resource/processquery/"/>
<link rel="appliquery" href="config/112761542179152739/project/215358/resource/appliquery/"/>
<link rel="child_project" href="config/112761542179152739/project/215358/child/"/>
<link rel="collection" href="config/112761542179152739/project/215358/collection/"/>
<link rel="file" href="live/112761542179152739/project/215358/upload/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<link rel="versionedfile" href="config/112761542179152739/project/215358/file/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<category term="project_type" label="ADV"/>
<author>
<name>Bellucci, Cristiano</name>
<uri>config/112761542179152739/user/993079</uri>
</author>
<id>215358</id>
<rights>
[WRITE_PRODUCTION_VERSION, WRITE_RESOURCE, WRITE_TRANSLATION, CREATE_VERSION_MANAGEMENT, READ_VERSION_MANAGEMENT, VERSIONABLE, READ]
</rights>
<updated>2018-11-21T13:03:15Z</updated>
<published>2018-11-20T07:53:42Z</published>
<content type="xhtml" src="config/112761542179152739/project/215358"/>
<summary type="html"/>
</entry>
<entry>
<title>CWL Eng Release</title>
<link rel="version" href="config/112761542179152739/project/215400/version/"/>
<link rel="process_as_service" href="config/112761542179152739/service?filter=TYPE+PROJECT&operator=EE+EE&value=PRO+215400"/>
<link rel="stat" href="config/112761542179152739/project/215400/stat/"/>
<link rel="widget_appli" href="config/112761542179152739/appli?filter=SUBTYPE+PROJECT&operator=IN+EE&value=WIDGET+215400"/>
<link rel="appli" href="config/112761542179152739/appli?filter=PROJECT&operator=EE&value=215400"/>
<link rel="process" href="config/112761542179152739/process?filter=PROJECT&operator=EE&value=215400"/>
<link rel="host" href="config/112761542179152739/host?filter=PROJECT&operator=EE&value=215400"/>
<link rel="customlist" href="live/112761542179152739/data?filter=PROJECT&operator=EE&value=215400"/>
<link rel="uploaded_images" href="live/112761542179152739/project/215400/upload?filter=TYPE&operator=EE&value=image"/>
<link rel="versioned_images" href="config/112761542179152739/project/215400/file/?filter=TYPE&operator=EE&value=image"/>
<link rel="file_by_extension" href="live/112761542179152739/project/215400/upload?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="versionedfile_by_extension" href="config/112761542179152739/project/215400/file/?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="service" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215400+EXT"/>
<link rel="api" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215400+COMP"/>
<link rel="processquery" href="config/112761542179152739/project/215400/resource/processquery/"/>
<link rel="appliquery" href="config/112761542179152739/project/215400/resource/appliquery/"/>
<link rel="child_project" href="config/112761542179152739/project/215400/child/"/>
<link rel="collection" href="config/112761542179152739/project/215400/collection/"/>
<link rel="file" href="live/112761542179152739/project/215400/upload/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<link rel="versionedfile" href="config/112761542179152739/project/215400/file/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<category term="project_type" label="ADV"/>
<author>
<name>Bellucci, Cristiano</name>
<uri>config/112761542179152739/user/993079</uri>
</author>
<id>215400</id>
<rights>
[WRITE_PRODUCTION_VERSION, WRITE_RESOURCE, WRITE_TRANSLATION, CREATE_VERSION_MANAGEMENT, READ_VERSION_MANAGEMENT, VERSIONABLE, READ]
</rights>
<updated>2018-11-21T12:06:18Z</updated>
<published>2018-11-20T07:53:09Z</published>
<content type="xhtml" src="config/112761542179152739/project/215400"/>
<summary type="html"/>
</entry>
<entry>
<title>CWL Eng Project</title>
<link rel="version" href="config/112761542179152739/project/215449/version/"/>
<link rel="process_as_service" href="config/112761542179152739/service?filter=TYPE+PROJECT&operator=EE+EE&value=PRO+215449"/>
<link rel="stat" href="config/112761542179152739/project/215449/stat/"/>
<link rel="widget_appli" href="config/112761542179152739/appli?filter=SUBTYPE+PROJECT&operator=IN+EE&value=WIDGET+215449"/>
<link rel="appli" href="config/112761542179152739/appli?filter=PROJECT&operator=EE&value=215449"/>
<link rel="process" href="config/112761542179152739/process?filter=PROJECT&operator=EE&value=215449"/>
<link rel="host" href="config/112761542179152739/host?filter=PROJECT&operator=EE&value=215449"/>
<link rel="customlist" href="live/112761542179152739/data?filter=PROJECT&operator=EE&value=215449"/>
<link rel="uploaded_images" href="live/112761542179152739/project/215449/upload?filter=TYPE&operator=EE&value=image"/>
<link rel="versioned_images" href="config/112761542179152739/project/215449/file/?filter=TYPE&operator=EE&value=image"/>
<link rel="file_by_extension" href="live/112761542179152739/project/215449/upload?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="versionedfile_by_extension" href="config/112761542179152739/project/215449/file/?filter=EXTENSION&operator=IN&value={extension}"/>
<link rel="service" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215449+EXT"/>
<link rel="api" href="config/112761542179152739/service?filter=PROJECT+TYPE&operator=EE+EE&value=215449+COMP"/>
<link rel="processquery" href="config/112761542179152739/project/215449/resource/processquery/"/>
<link rel="appliquery" href="config/112761542179152739/project/215449/resource/appliquery/"/>
<link rel="child_project" href="config/112761542179152739/project/215449/child/"/>
<link rel="collection" href="config/112761542179152739/project/215449/collection/"/>
<link rel="file" href="live/112761542179152739/project/215449/upload/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<link rel="versionedfile" href="config/112761542179152739/project/215449/file/?filter=VISIBILITY&operator=IN&value={visibility}"/>
<category term="project_type" label="ADV"/>
<author>
<name>Bellucci, Cristiano</name>
<uri>config/112761542179152739/user/993079</uri>
</author>
<id>215449</id>
<rights>
[WRITE_PRODUCTION_VERSION, WRITE_RESOURCE, WRITE_TRANSLATION, CREATE_VERSION_MANAGEMENT, READ_VERSION_MANAGEMENT, VERSIONABLE, READ]
</rights>
<updated>2018-11-20T15:47:48Z</updated>
<published>2018-11-20T07:52:38Z</published>
<content type="xhtml" src="config/112761542179152739/project/215449"/>
<summary type="html"/>
</entry>
</feed>
```

As you can see it is quite extensive. The server needs to continuously serve data to our UI so it cannot wait for this API to be executed it all has to happen ASYNCHRONOUSLY. Now there are async libraries in C#, Unity and many more and lets not forget Java is multithreaded! But node is built on asnyc programming unlike these others and its implementation of promises and callbacks made it so much easier to work with. The client API’s need to continuously step further in making more and more requests and often we need to wait for one to come back before we can make another. Promise code such as below is vital here and Node is brilliant at it.

```
firebase.database().ref('node-client/images').once('value').then(function (snapshot) {
   rooms.push(snapshot.val());
})
   // Create list of rooms and get the one we want from the request ID
   .then(function () {
       Promise.all(rooms).then(values => {
           Object.values(values).forEach(function (data) {
               Object.values(data).forEach(function (room) {
                   if (room.MetaData === requestId) {
                       requestedRoom = room;
                   }
                   else {
                       jsonRooms.push(room);
                   }
               });
           });
```

Lastly Node is javascript, so It is also loosely typed which makes it possible to continue to assign variables regardless of type. This made the code much easier to write especially with regards to the API’s when we don’t know what data is coming back.

### Why Express?

Express just made it easy for us to get developing. It is easy to use with a very small learning curve and removes the need of boilerplate code and middleware to start making requests. It makes it easier than using plain Node.js as we can gain more information on the requests and have separate handlers for get, post and more. It could also provide routing for future implementations. We had already worked with it before so it seemed like a good choice?

On the whole yes, as explained it did what we needed in the short time frame. But there are certainly better frameworks and we will be using them in the future. Koa is a better alternative and aims to 
‘fix and replace node’ unlike Express which ‘augments node’. As well as this Koa uses promises and async functions by default and has much better error handling. It would have been better for our API’s as it dosn't carry the beef that Express does with features such as routing and templating; which we didn’t need.

### Firebase

We used Firebase as our database to store information on buildings, rooms and employees.

We chose it based on the reasons below:

* it is NoSQL and works with JSON - no barrier between data and objects
* minimal setup with no schemas
* easy access to data, files, auth, and more
* no server infrastructure needed to power apps with data
* real time
* highly secure with inbuilt authenticaion
* Can be used in future for logins
* serverless

However if relational tables are needed then Firebase is not a good solution and SQL should be used instead. Firebase can also get expensive with lots of data and you are tied into the Google ecosystem when using it.

Information from [here](https://medium.com/one-tap-software/firebase-pros-and-cons-ce37c766190a)

Learn how to create your own Firebase database [here](https://firebase.google.com/docs/database/admin/save-data) and [here](https://firebase.google.com/docs/database/)


## Deployment

To deploy the app you need a way of hosting the server and UI. I chose AWS.

Create an instance on AWS for the react app and server and then put the ip address of the instance in the relevant places so that the app points to these servers. This includes the post of the room form and the socket api's.

Form post - src/components/form:

`const BASE_URL = 'http://awsipAddress.com:4000/';`

Sockets - src/api.js:

`const socket = openSocket('http://awsAddress.com:4000/';)`

Once the instance is created, you need to get the project on it. Download an SSH Key from the AWS Instance website as deminstrated in the link.

https://aws.amazon.com/getting-started/projects/deploy-nodejs-web-app/

Then using a terminal in the same directory as the key you have downloaded, follow the steps below to configure and start the project.

###### SSH Into the server
```
ssh -i ReactNodeServerKey.pem ubuntu@ec2-18-130-43-56.eu-west-2.compute.amazonaws.com
```

###### Install node.js
```curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

###### |Install Git
```
sudo apt-get install git
```

###### Install npm
```
sudo npm install
```

###### Start on port 80 - this is for development purposes only
###### Port 80 should not be used in future
Also Run the command 'screen' and hit enter - this runs the process forever

```
screen
sudo PORT=80 npm start
```

###### Set permissions to upload folder so images can be added
```
sudo chown -R $USER:$GROUP public/images/uploads
```

###### Update
```
sudo apt-get update
```

###### Clone the project

```
git clone git@gitlab.cs.cf.ac.uk:c1673107/react-project-group2.git
```

###### Start server
```
cd /src/server
node server
```

## Using the API

The rooms API takes in an ID and returns the processes for a room based on the ID. To use this API, follow the route http://localhost:4000/api/rooms?id=ID in a web browser. The ID of a room is in the metadata of each upload. This API returns JSON such as this:

```
  "room": "CWL HR",
  "employees": [
    {
      "active": true,
      "id": 1,
      "name": "Jack Bennet"
    },
    {
      "active": false,
      "id": 2,
      "name": "Tom Leaht"
    }
  ],
  "closestRoom": {
    "name": "Testing",
    "direction": "North West"
  },
```

## Adding Screen Reader

To use this application with a screen reader, add the chrome extension 'ChromeVox'
Once installed, click on the Chromevox logo next to the navigation bar > options.
Enter the following shortcuts for the following actions:
Click on current item > B
Next button > N

The other commands used to navigate this application via chromevox should already be allocated.
You can also create your own keyboard commands that suit your.

Refer to the following commands to learn how to navigate the application.

## Keyboard commands
### `Navigate through elements : TAB`
### `Select room from grid: B`
### `Go back to previous element: SHIFT-TAB`
### `Go to next button: N`
### `Exit the modal: EXIT`

## Built With

* [npm](https://www.npmjs.com/) - The package manager 
* [React](https://reactjs.org/) - The JavaScript library for building the user interface
* [Node](https://nodejs.org/en/) - Used for the server to host the API's and send client data

## Versioning

The live version is currently on 1.3

## Authors

* **Jack Allcock** 
* **Reagan Vose**
* **Ieuan Jones**
* **Daniel Duggan**

## Third party packages and software used

| Plugin | Explanation | 
| ------ | ------ |
| MaterialUI | We used the material ui framework to allow us to create a quick yet proffesional UI. We used it for the Nav bar, Forms, Modal, Buttons and Tables. | 
| socket.io | The socket.io library allows real time data to stream from the firebase database to the UI. If there is a change to a room, the data changes on the page without a reload. |
| socket.io-client | The socket client allows communication between the server and client|
| @fortawesome | Fort awesome provides nice icons and fonts for a proffessional UI.   |
| axios | The Axios library is a way to post from react to a server using Ajax. It was vital for submitting data to firebase|
| cors | the cors library provides quick and easy middleware to setup Cross-Origin Resource Sharing between the UI and the node server |
| express | The express framework allowed us to create post and get API's quickly |
| firebase | We chose firebase as our storage database due to it being on the cloud which means we didn't have to host it. Also it is quick and easy to setup as data can be added on the fly and no tables need to be specified. |
| multer |  The multer library makes uploading images to the server easy and fast|
| xml2js | This library converts the XML from Fujitsu's API's into JSON. This is more cross-server friendly and core node dosn't support XML|
| enzyme | AirBnb testing library |

[Create React App](https://reactjs.org/docs/create-a-new-react-app.html) - used to boostrap the project
[Jest](https://jestjs.io/) - used to write tests


## Acknowledgments

Thank you to Fujitsu and RunMyProcess for allowing us to build this product prototype for them, we hope it has given them a good idea of what they want/is acheviable. 




   

