/*
  Created by C1673107
  Dependencies:
    - Modal.js
    - data from sockets in api.js
    - /images/loading.gif
    - buildingId supplied as prop from navigator.js->app.js->here
    - darkMode prop from navigator.js->app.js->here
  Usage: This content page is the main content. It features the table for the rooms of a building and the buttons to add a room via the modal
  Status: Finished
  Libraries: The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import RoomsTable from "./roomsTable";
import Modal from "./modal";
import { subscribeToData } from "../api";
import Loading from "../images/loading.gif";

// Styles from Material UI - https://material-ui.com/demos/tables/
const styles = theme => ({
    paper: {
        maxWidth: '80%',
        margin: 'auto',
        overflow: 'hidden',
    },
    block: {
        display: 'block',
    },
    contentWrapper: {
        margin: '40px 16px',
    },
});

class Content extends React.Component {

    // Sets data to the state from the sockets
    constructor(props) {
        super(props);
        subscribeToData((err, data) => this.setState({
            data
        }));
    }

    state = {
        value: 0,
        data: 0,
    };

    render() {
        // Only load content if we have data, i.e, a building ID, otherwise load a message to add data
        let loadContent = false;
        // Building ID passed from navigator.js->app.js->here
        let buildingId = this.props.buildingId;
        // To load the table if we have data, i.e, rooms.
        let hasData = false;
        // Rooms to pass to the table component
        let roomsToSend = [];
        // If we have a building ID load content
        if (buildingId !== 'undefined') {
            // If we have data, load the form
            if (typeof this.state.data !== 'undefined' && this.state.data) {
                Object.values(this.state.data).forEach(function (building) {
                    // Only send rooms with that building ID
                    // Building ID might be a string, so no ===
                    if (building.BuildingID == buildingId) {
                        roomsToSend.push(building);
                        hasData = true;
                    }
                });
            }
            loadContent = true;
        }

        // Change the text colour if dark mode is enabled
        let textColour;
        if (this.props.darkMode) {
            textColour = 'primary'
        }
        else {
            textColour = 'textSecondary'
        }

    return (
      <div>
        <div>
          {loadContent > 0 ? (
            <div>
              <div>
                {hasData > 0 ? (
                  <div>
                    <RoomsTable data={roomsToSend} buildingId={buildingId} />
                  </div>
                ) : (
                  <div>
                    <Typography color={textColour} align="center" tabIndex="0">
                      There are no rooms created for this building
                    </Typography>
                  </div>
                )}
                <br />
                <br />
                <br />
                <center>
                  <Modal form={this.props.form} />
                </center>
              </div>
            </div>
          ) : (
            <div className="loading">
              <Typography color={textColour} align="center" tabIndex="0">
                <h3>Click on a building to load room data!</h3>
                <h5>If there are no buildings, create a new one</h5>
                <br />
              </Typography>
              <center>
                <img
                  src={Loading}
                  tabIndex="0"
                  title="loading animation"
                  alt="loading animation"
                />
              </center>
            </div>
          )}
        </div>
      </div>
    );
  }
}

Content.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Content);