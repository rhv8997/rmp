/*
  Created by C1673107
  Dependencies:
    - Grid.js
    - data from sockets in api.js
    - buildingId supplied as prop from navigator.js->app.js->here
    - darkMode prop from navigator.js->app.js->here
  Usage: This form is for adding rooms to Firebase. It pulls data from sockets to create list of processes and employees to chose from
  Status: Finished
  Libraries:
    - The HTML components are from the Material UI framework - https://material-ui.com/
    - Axios - https://medium.com/@harinilabs/day-5-making-server-request-in-react-with-axios-8e85549caf62
    - Form Data - https://www.npmjs.com/package/form-data
 */

import React from "react";
import { subscribeToData2 } from "../api";
import { subscribeToData3 } from "../api";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Grid from "./grid";
import Button from "@material-ui/core/Button/Button";
import SaveIcon from "@material-ui/icons/Save";
import axios from "axios";
const BASE_URL = 'http://ec2-18-130-43-56.eu-west-2.compute.amazonaws.com:4000/';

// Styles for the form from Material UI - https://material-ui.com/demos/tables/
const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  dropDown: {
    marginTop: -15
  },
  dense: {
    marginTop: 10
  },
  menu: {
    width: 280
  },
  button: {
    margin: theme.spacing.unit
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  }
});

class OutlinedTextFields extends React.Component {
  constructor(props) {
    super(props);
    // Process data from sockets, set state
    subscribeToData2((err, data2) =>
      this.setState({
        data2
      })
    );
    // Employee data from sockets, set state
    subscribeToData3((err, data3) =>
      this.setState({
        data3
      })
    );
  }

  state = {
    name: "",
    buildingId: 0,
    file: "",
    location: 0,
    images: [],
    imageUrls: [],
    message: "",
    processes: [],
    employees: [],
    data2: [],
    data3: [],
    ids: [],
    employeeIds: []
  };
  // This method gets all the images form the form input and puts in an array and sets state
  selectImages = event => {
    let images = [];
    for (let i = 0; i < event.target.files.length; i++) {
      images[i] = event.target.files.item(i);
    }
    this.setState({ images });
  };
  // This method gets the processes amd compiles a dict of process names and ID's
  uploadImages = () => {
    let processObjects = this.state.processes;
    let processesToSend = [];
    Object.keys(processObjects).forEach(function(key) {
      let val = processObjects[key];
      let data = {
        processName: val.processName,
        id: val.id
      };
      processesToSend.push(JSON.stringify(data));
    });
    // Packages all the data together and adds to form data
    // The form data is then posted to the server via the UR
    // Uses Form Data - https://www.npmjs.com/package/form-data
    const uploads = this.state.images.map(image => {
      const data = new FormData();
      data.append("image", image, image.name);
      data.append("name", this.state.name);
      data.append("location", this.state.location);
      data.append("employees", this.state.employeeIds);
      data.append("buildingId", this.state.buildingId);
      data.append("processes", String(processesToSend));
      // Make an AJAX upload request using Axios
      return axios.post(BASE_URL + "upload", data).then(response => {
        this.setState({
          imageUrls: [response.data.imageUrl, ...this.state.imageUrls]
        });
      });
    });

    // Alert image upload success
    axios
      .all(uploads)
      .then(() => {
        alert("Image added successfully");
        {
          window.location.reload();
        }
      })
      .catch(err => alert(err.message));
  };

  // This callback returns the selected grid from grid.js
  gridCallback = dataFromChild => {
    this.setState({
      location: dataFromChild
    });
  };

  // This takes in the input name and sets state for it
  handleChange = name => event => {
    if (name === "file") {
      this.setState({
        [name]: event.target.files[0]
      });
    }
    this.setState({
      [name]: event.target.value
    });
  };

  // Adds employees selected to list
  save = name => event => {
    if (name === "employees") {
      let dataIn = event.target.value;
      let data = this.state.employees;
      let ids = this.state.employeeIds;

      if (!ids.includes(dataIn.id)) {
        data.push(dataIn);
        ids.push(dataIn.id);
      }
      this.setState({
        [name]: data
      });
    } else {
      let dataIn = event.target.value;
      let data = this.state.processes;
      let ids = this.state.ids;

      if (!ids.includes(dataIn.id)) {
        data.push(dataIn);
        ids.push(dataIn.id);
      }
      this.setState({
        [name]: data
      });
    }
  };

  render() {
    const { classes } = this.props;
    console.log(this.state.data3);
    return (
      <div>
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            required
            id="outlined-name"
            label="Room name"
            className={classes.textField}
            value={this.state.name}
            onChange={this.handleChange("name")}
            margin="normal"
            variant="outlined"
          />
          <TextField
            required
            id="outlined-uncontrolled"
            label="Building Number"
            defaultValue=""
            className={classes.textField}
            onChange={this.handleChange("buildingId")}
            margin="normal"
            variant="outlined"
          />
        </form>
        <hr />
        <div className="projects-tab">
          <label>
            <h5>Select Processes</h5>
            <TextField
              id="standard-select-currency"
              select
              className={classes.dropDown}
              value={this.state.currency}
              onChange={this.save("processes")}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              helperText="Please select the processes"
              margin="normal"
            >
              {this.state.data2.map(option => (
                <MenuItem key={option.id} value={option}>
                  {option.processName}
                </MenuItem>
              ))}
            </TextField>
            {this.state.processes.map(function(item) {
              return <li>{item.processName}</li>;
            })}
          </label>
          <label>
            <h5>Select Employees</h5>
            <TextField
              id="standard-select-employees"
              select
              className={classes.dropDown}
              value={this.state.employees}
              onChange={this.save("employees")}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              helperText="Please select the employees"
              margin="normal"
            >
              {this.state.data3.map(option => (
                <MenuItem key={option.id} value={option}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            {this.state.employees.map(function(item) {
              return <li>{item.name}</li>;
            })}
          </label>
        </div>
        <label>
          <h5>Select Upload Image</h5>
          <input
            className="form-control "
            type="file"
            onChange={this.selectImages}
          />
        </label>
        <label>
          <h5 tabIndex="0">Select Room Location</h5>
          <Grid callbackFromParent={this.gridCallback} />
        </label>
        <br />
        <Button
          variant="contained"
          color="primary"
          onClick={this.uploadImages}
          className={classes.buttons}
        >
          Save
          <SaveIcon
            className={classNames(classes.rightIcon, classes.iconSmall)}
          />
        </Button>
      </div>
    );
  }
}

OutlinedTextFields.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OutlinedTextFields);
