/*
  Created by C1673107
  Dependencies: callback prop from form.js->app.js->here
  Usage: This grid gives ability to add a location for a room. It must pass the selected cell back to the form
  Status: Finished
  Libraries: None
 */

import React, { Component } from "react";
import "../App.css";

class Grid extends Component {
  constructor(props) {
    super(props);
    // State to change cell colour if change -> saves cell number here
    this.state = {
      change: 0
    };
  }

  // If cell clicked, set state and pass to callback to tell the form
  handleClick(value) {
    this.setState({ change: value });
    this.props.callbackFromParent(value);
  }

  // Create a grid based on size, default is 10x10
  createGrid(size) {
    let m = [];
    let currentNumber = 1;
    for (let i = 0; i < size; i++) {
      m[i] = [];
      for (let j = 0; j < size; j++) m[i][j] = currentNumber++;
    }
    return m;
  }

  // Change colour of a cell if it is clicked
  render() {
    let board = this.createGrid(10);
    return (
      <table className="grid" tabindex="0">
        <tbody>
          {board.map((row, index) => {
            return (
              <tr key={"row_" + index}>
                {row.map((cell, index) => {
                  return cell === this.state.change ? (
                    <td
                      tabindex="0"
                      key={"cell_" + index}
                      style={{ backgroundColor: "#243e56", color: "white" }}
                      onClick={() => this.handleClick(cell)}
                    >
                      {cell}
                    </td>
                  ) : (
                    <td
                      tabindex="0"
                      key={"cell_" + index}
                      style={{ backgroundColor: "white" }}
                      onClick={() => this.handleClick(cell)}
                    >
                      {cell}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default Grid;
