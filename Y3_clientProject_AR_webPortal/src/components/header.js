/*
  Created by C1673107
  Dependencies:
    - headerBackground prop from navigator.js->app.js->here
  Usage: This header sits at the top of content and has the profile and help button. It also hosts the NAV bar sandwhich on mobile mode
  Status: Finished
  Libraries:
    - The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import HelpIcon from '@material-ui/icons/Help';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import AvatarImage from '../images/avatar.jpg'

const lightColor = "rgba(255, 255, 255, 0.7)";

// Styles from https://github.com/mui-org/material-ui/tree/master/docs/src/pages/premium-themes
const styles = theme => ({
  secondaryBar: {
    zIndex: 0
  },
  menuButton: {
    marginLeft: -theme.spacing.unit
  },
  iconButtonAvatar: {
    padding: 10
  },
  link: {
    textDecoration: "none",
    color: lightColor,
    "&:hover": {
      color: theme.palette.common.white
    }
  },
  button: {
    borderColor: lightColor
  }
});

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  
  // Drawer from from https://github.com/mui-org/material-ui/tree/master/docs/src/pages/premium-themes
  // Passes in the header background from props if dark mode enabled

  render() {
    const { classes, onDrawerToggle } = this.props;

    return (
      <React.Fragment>
        <AppBar
          color={this.props.style.headerBackground}
          position="sticky"
          elevation={0}
        >
          <Toolbar>
            <Grid container spacing={8} alignItems="center">
              <Hidden smUp>
                <Grid item>
                  <IconButton
                    color="inherit"
                    aria-label="Open drawer"
                    onClick={onDrawerToggle}
                    className={classes.menuButton}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>
              </Hidden>
              <Grid item xs />
              <Grid item>
                <br />
                <Tooltip title="Help button" alt="help button">
                  <IconButton color="inherit">
                    <HelpIcon />
                  </IconButton>
                </Tooltip>
              </Grid>
              <Grid item>
                <br />
                <IconButton
                  color="inherit"
                  className={classes.iconButtonAvatar}
                >
                  <Avatar
                    className={classes.avatar}
                    src={AvatarImage}
                    title="account avatar"
                    alt="account avatar"
                  />
                </IconButton>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired
};

export default withStyles(styles)(Header);