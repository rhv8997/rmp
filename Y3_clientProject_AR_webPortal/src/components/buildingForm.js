/*
  Created by C1673107
  Dependencies: None
  Usage: This is the building form which will allow users to add buildings to Firebase
  Status: Not finished - Needs link to backend uploader
  Libraries: The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button/Button";
import SaveIcon from '@material-ui/icons/Save';

// Styles for the form from Material UI - https://material-ui.com/demos/text-fields/
const styles = theme => ({
    container: {
        display: "flex",
        flexWrap: "wrap"
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    },
    dropDown: {
        marginTop: -15
    },
    dense: {
        marginTop: 10
    },
    menu: {
        width: 280,
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    },
});

class OutlinedTextFields extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <form className={classes.container} noValidate autoComplete="off">
                    <TextField
                        required
                        id="outlined-name"
                        label="Building name"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        required
                        id="outlined-uncontrolled"
                        label="Building Location"
                        defaultValue=""
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />

                </form>

                <br/>

                <Button variant="contained" color="primary" className={classes.buttons}>
                    Save
                    <SaveIcon className={classNames(classes.rightIcon, classes.iconSmall)} />
                </Button>
            </div>

        );
    }
}

OutlinedTextFields.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OutlinedTextFields);
