/*
  Created by C1673107
  Dependencies:
    - form.js
    - buildingForm.js
    - prop from navigator and content which decides which form to hosr
  Usage: This modal hosts the forms for rooms and buildings
  Status: Finished
  Libraries:
    - The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import classNames from "classnames";
import Form from "./form";
import BuildingForm from "./buildingForm";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import AddIcon from "@material-ui/icons/Add";

// Styles from Material UI - https://material-ui.com/utils/modal/

// Styles for the modal
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: "50%",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  },
  button: {
    margin: theme.spacing.unit
  },
  buttons: {
    margin: "10%",
    marginTop: 40
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  }
});

class SimpleModal extends React.Component {
  constructor(props) {
    super(props);
  }

  // State for open/closed and which form to host
  state = {
    open: false,
    form: false
  };

  // Decides whether to open or close
  // https://material-ui.com/utils/modal/
  handleOpen = type => {
    this.setState({ open: true });
    this.setState({ form: type });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    // https://material-ui.com/utils/modal/
    // Load form based on prop
    return (
      <div>
        {this.props.form == "building" ? (
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleOpen("building")}
              className={classes.buttons}
            >
              New Building
              <AddIcon
                className={classNames(classes.rightIcon, classes.iconSmall)}
              />
            </Button>
            <br />
            <Modal
              aria-labelledby="simple-modal-title"
              open={this.state.open}
              onClose={this.handleClose}
            >
              <div style={getModalStyle()} className={classes.paper}>
                <div>
                  <h4 tabindex="0">Add Building Details</h4>
                  <BuildingForm />
                </div>
              </div>
            </Modal>
          </div>
        ) : (
          <div>
            <Button
              onClick={() => this.handleOpen("room")}
              variant="contained"
              color="default"
              className={classes.buttons}
            >
              Create new room
              <CloudUploadIcon className={classes.rightIcon} />
            </Button>
            <br />
            <Modal
              aria-labelledby="simple-modal-title"
              open={this.state.open}
              onClose={this.handleClose}
            >
              <div>
                <div style={getModalStyle()} className={classes.paper}>
                  <h4 tabindex="0">Add Room Details</h4>
                  <Form />
                </div>
              </div>
            </Modal>
          </div>
        )}
        ;
      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;
