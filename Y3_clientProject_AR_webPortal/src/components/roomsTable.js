/*
  Created by C1673107
  Dependencies:
    - Material UI
  Usage: This room table loads all of the rooms into rows
  Status: Finished
  Libraries: The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

// Styles from Material UI - https://material-ui.com/demos/tables/
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#CAC9C5",
    color: theme.palette.common.black
  },
  body: {
    fontSize: 15
  }
}))(TableCell);

// Styles from Material UI - https://material-ui.com/demos/tables/
const styles = theme => ({
  root: {
    width: "80%",
    marginTop: "1%",
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: "#dedfe0"
    }
  },
  paper: {
    maxWidth: "80%",
    margin: "auto",
    overflow: "hidden"
  },
  block: {
    display: "block"
  }
});

// Styles from Material UI - https://material-ui.com/demos/tables/
// Create rows and add ID
let id = 0;
function createData(name, buildingId, image, metaData, location, processes) {
  id += 1;
  return { id, name, buildingId, image, metaData, location, processes };
}

function SimpleTable(props) {
  const rows = [];
  const { classes } = props;
  let data = props.data;
  let processes;
  let meta;
  // If we have data from firebase, starting loading data
  if (data !== "Anonymous") {
    // Get all processess and metadata
    Object.values(data).map((e, i) => {
      let processRows = [];
      let metaData = [];
      processes = e.Processes;
      meta = e.MetaData;
      console.log(processes);
      processes = JSON.parse("[" + processes + "]");
      Object.values(processes).map((e, i) => {
        processRows.push(e);
      });
      rows.push(
        createData(
          e.Name,
          e.BuildingID,
          e.Image,
          metaData,
          e.Location,
          processRows
        )
      );
    });
  }

  return (
    <div>
      {rows.length > 0 ? (
        <div>
          <Paper className={classes.paper}>
            <Table className={classes.table} tabindex="0">
              <TableHead>
                <TableRow>
                  <CustomTableCell
                    style={{ textAlign: "left" }}
                    numeric
                    tabindex="0"
                  >
                    ID
                  </CustomTableCell>
                  <CustomTableCell
                    style={{ textAlign: "left" }}
                    numeric
                    tabindex="0"
                  >
                    Room Name
                  </CustomTableCell>
                  <CustomTableCell
                    style={{ textAlign: "left" }}
                    numeric
                    tabindex="0"
                  >
                    Location
                  </CustomTableCell>
                  <CustomTableCell
                    style={{ textAlign: "left" }}
                    numeric
                    tabindex="0"
                  >
                    Attached Processes
                  </CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map(row => {
                  return (
                    <TableRow className={classes.row} key={row.id}>
                      <CustomTableCell
                        style={{ textAlign: "left" }}
                        numeric
                        tabindex="0"
                      >
                        {row.image}
                      </CustomTableCell>
                      <CustomTableCell
                        style={{ textAlign: "left" }}
                        numeric
                        tabindex="0"
                      >
                        {row.name}
                      </CustomTableCell>
                      <CustomTableCell
                        style={{ textAlign: "left" }}
                        numeric
                        tabindex="0"
                      >
                        {row.location[0] + "," + row.location[1]}
                      </CustomTableCell>
                      <CustomTableCell
                        style={{ textAlign: "left" }}
                        numeric
                        tabindex="0"
                      >
                        {row.processes.map(
                          process => process.processName + ", "
                        )}
                      </CustomTableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>
      ) : (
        <div>
          <br />
          {/*<h3>There are no rooms added</h3>*/}
          <br />
        </div>
      )}
    </div>
  );
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleTable);
