/*
  Created by C1673107
  Dependencies:
    - Modal.js
    - Material UI Nav https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase/Navigator.js
    - Logo image
    - Building data from sockets at api.js
  Usage: This nav is a side bar that collapses and expands for mobile. It pulls in all the buildings, allowing you to click on them
  in order to view the rooms. It also has settings on it for accessibility.
  Status: Finished
  Libraries: The HTML components are from the Material UI framework - https://material-ui.com/
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Buildings from '@material-ui/icons/Business';
import purple from '@material-ui/core/colors/purple';
import Logo from '../images/logo.JPG'
import Switch from '@material-ui/core/Switch';
import {subscribeToBuildingData} from "../api";
import Modal from "./modal";
import Typography from "@material-ui/core/Typography/Typography";

// Styles from https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase/Navigator.js
const styles = theme => ({
  categoryHeader: {
    paddingTop: 30,
    paddingBottom: 20
  },
  categoryHeaderPrimary: {
    color: theme.palette.common.white
  },
  categoryHeaderSecondary: {
    color: "#000000",
    fontFamily: "Comic Sans MS",
    fontSize: 22
  },
  buttons: {
    margin: "10%",
    marginTop: 40
  },
  item: {
    paddingTop: 4,
    paddingBottom: 4,
    color: "rgba(255, 255, 255, 0.7)"
  },
  itemCategory: {
    backgroundColor: "#000000",
    boxShadow: "0 -1px 0 #404854 inset",
    paddingTop: 16,
    paddingBottom: 16
  },
  firebase: {
    fontSize: 24,
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.common.white
  },
  settings: {
    paddingTop: 50,
    paddingBottom: 16,
    fontSize: 18
  },
  settingsSecondary: {
    paddingTop: 50,
    paddingBottom: 16,
    fontSize: 20,
    color: "#000000"
  },
  settingsEntry: {
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: 15
  },
  itemActionable: {
    "&:hover": {
      backgroundColor: "rgba(255, 255, 255, 0.08)"
    }
  },
  itemActiveItem: {
    color: "#F0C105"
  },
  itemActionableSecondary: {
    "&:hover": {
      backgroundColor: "#18202c"
    }
  },
  itemActiveItemSecondary: {
    color: "#18202c"
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  },
  itemPrimary: {
    color: "inherit",
    fontSize: theme.typography.fontSize,
    "&$textDense": {
      fontSize: theme.typography.fontSize
    }
  },
  itemSecondary: {
    paddingTop: 4,
    paddingBottom: 4,
    color: "#000000",
    fontFamily: "Comic Sans MS",
    fontSize: 20
  },
  itemClickedSecondary: {
    paddingTop: 4,
    paddingBottom: 4,
    color: "#F0C105",
    fontFamily: "Comic Sans MS",
    fontSize: 20
  },
  textDense: {},
  divider: {
    marginTop: theme.spacing.unit * 2
  },
  colorSwitchBase: {
    color: purple[300],
    "&$colorChecked": {
      color: purple[500],
      "& + $colorBar": {
        backgroundColor: purple[500]
      }
    }
  },
  colorBar: {},
  colorChecked: {},
  iOSSwitchBase: {
    "&$iOSChecked": {
      color: theme.palette.common.white,
      "& + $iOSBar": {
        backgroundColor: "#F0C105"
      }
    },
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.sharp
    })
  },
  iOSChecked: {
    transform: "translateX(15px)",
    "& + $iOSBar": {
      opacity: 1,
      border: "none"
    }
  },
  iOSBar: {
    borderRadius: 13,
    width: 42,
    height: 26,
    marginTop: -13,
    marginLeft: -21,
    border: "solid 1px",
    borderColor: theme.palette.grey[400],
    backgroundColor: theme.palette.grey[500],
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"])
  },
  iOSIcon: {
    width: 24,
    height: 24
  },
  iOSIconChecked: {
    boxShadow: theme.shadows[1]
  }
});

class Navigator extends React.Component {

    // Get building data for state
  constructor(props) {
    super(props);
    subscribeToBuildingData((err, buildingData) =>
      this.setState({
        buildingData: buildingData
      })
    );
  }

    state = {
        accessibilityMode: false,
        checkedA: false,
        buildingData: 0,
        error: 0,
        active: -1,
        checkedDarkMode: false
    };

    // Send building ID to app.js and then to content.js for rooms table
    // Change colour to yellow as active
    handleNav = (id) => {
        this.props.callbackFromParent(id);
        this.setState({ active: id });
    };

  handleDarkThemeSwitch = () => {
    if (this.state.checkedDarkMode) {
      this.props.callBackForDarkMode(false);
      this.setState({ checkedDarkMode: false });
    } else {
      this.setState({ checkedDarkMode: true });
      this.props.callBackForDarkMode(true);
    }
  };

  handleAccessiblitySwitch = () => {
    if (this.state.accessibilityMode) {
      this.props.accessiblityCallBack(false);
      this.setState({ accessibilityMode: false });
    } else {
      this.setState({ accessibilityMode: true });
      this.props.accessiblityCallBack(true);
    }
  };

  render() {
    const { classes, ...other } = this.props;

    return (
      <div>
        {this.state.accessibilityMode ? (
          <Drawer variant="permanent" {...other}>
            <List disablePadding>
              <ListItem
                className={classNames(
                  classes.firebase,
                  classes.item,
                  classes.itemCategory
                )}
              >
                <img
                  src={Logo}
                  width="100%;"
                  alt="run my process logo"
                  title="run my process logo"
                  tabIndex="0"
                />
              </ListItem>
              <React.Fragment key={1}>
                <br />
                <ListItem className={classes.categoryHeaderSecondary}>
                  <ListItemText
                    tabIndex="0"
                    classes={{
                      primary: classes.categoryHeaderSecondary
                    }}
                  >
                    Buildings
                  </ListItemText>
                </ListItem>
                {this.state.buildingData !== 0 ? (
                  <div>
                    {this.state.buildingData.map(building =>
                      this.state.active == building.id ? (
                        <ListItem
                          button
                          color="primary"
                          key={building.id}
                          value={building.name}
                          className={classNames(
                            classes.itemClickedSecondary,
                            classes.itemActionable,
                            classes.itemActiveItem
                          )}
                          onClick={() => this.handleNav(building.id)}
                        >
                          <ListItemIcon>
                            <Buildings />
                          </ListItemIcon>
                          <ListItemText
                            classes={{
                              primary: classes.itemClickedSecondary
                            }}
                          >
                            {building.name}
                          </ListItemText>
                        </ListItem>
                      ) : (
                        <ListItem
                          button
                          key={building.id}
                          value={building.name}
                          className={classNames(
                            classes.itemSecondary,
                            classes.itemActionable
                          )}
                          onClick={() => this.handleNav(building.id)}
                        >
                          <ListItemIcon>
                            <Buildings />
                          </ListItemIcon>

                          <ListItemText
                            classes={{
                              primary: classes.itemSecondary
                            }}
                          >
                            {building.name}
                          </ListItemText>
                        </ListItem>
                      )
                    )}
                  </div>
                ) : (
                  <div>
                    <Typography
                      color="textSecondary"
                      align="center"
                      tabIndex="0"
                    >
                      No data for this building yet
                    </Typography>
                  </div>
                )}
                <Divider className={classes.divider} />
              </React.Fragment>

              <Modal form="building" />
              <Divider className={classes.divider} />

              <ListItem
                className={classNames(classes.settingsSecondary)}
                tabIndex="0"
              >
                Settings
              </ListItem>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemSecondary,
                      textDense: classes.textDense
                    }}
                  >
                    Accessiblity Mode
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    // checked={this.state.checkedDarkMode}
                    onChange={this.handleAccessiblitySwitch}
                    value="checkedB"
                    id='accessiblitySwitch'
                  />
                </ListItem>
              </label>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemSecondary,
                      textDense: classes.textDense
                    }}
                  >
                    Dark Theme
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    checked={this.state.checkedDarkMode}
                    onChange={this.handleDarkThemeSwitch}
                    value="checkedB"
                  />
                </ListItem>
              </label>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemSecondary,
                      textDense: classes.textDense
                    }}
                  >
                    Increased Font
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    checked={this.state.checkedB}
                    // onChange={this.handleChange('checkedB')}
                    value="checkedB"
                  />
                </ListItem>
              </label>
            </List>
          </Drawer>
        ) : (
          <Drawer variant="permanent" {...other}>
            <List disablePadding>
              <ListItem
                className={classNames(
                  classes.firebase,
                  classes.item,
                  classes.itemCategory
                )}
              >
                <img
                  src={Logo}
                  width="100%;"
                  alt="run my process logo"
                  title="run  my process logo"
                  tabIndex="0"
                />
              </ListItem>
              <React.Fragment key={1}>
                <ListItem className={classes.categoryHeader}>
                  <ListItemText
                    tabIndex="0"
                    classes={{
                      primary: classes.categoryHeaderPrimary
                    }}
                  >
                    Buildings
                  </ListItemText>
                </ListItem>
                {this.state.buildingData !== 0 ? (
                  <div>
                    {this.state.buildingData.map(building =>
                      this.state.active == building.id ? (
                        <ListItem
                          button
                          dense
                          color="primary"
                          key={building.id}
                          value={building.name}
                          className={classNames(
                            classes.item,
                            classes.itemActionable,
                            classes.itemActiveItem
                          )}
                          onClick={() => this.handleNav(building.id)}
                        >
                          <ListItemIcon>
                            <Buildings />
                          </ListItemIcon>
                          <ListItemText
                            classes={{
                              primary: classes.itemPrimary,
                              textDense: classes.textDense
                            }}
                          >
                            {building.name}
                          </ListItemText>
                        </ListItem>
                      ) : (
                        <ListItem
                          button
                          dense
                          key={building.id}
                          value={building.name}
                          className={classNames(
                            classes.item,
                            classes.itemActionable
                          )}
                          onClick={() => this.handleNav(building.id)}
                        >
                          <ListItemIcon>
                            <Buildings />
                          </ListItemIcon>

                          <ListItemText
                            classes={{
                              primary: classes.itemPrimary,
                              textDense: classes.textDense
                            }}
                          >
                            {building.name}
                          </ListItemText>
                        </ListItem>
                      )
                    )}
                  </div>
                ) : (
                  <div>
                    <Typography
                      color="textSecondary"
                      align="center"
                      tabIndex="0"
                    >
                      No data for this building yet
                    </Typography>
                  </div>
                )}
                <Divider className={classes.divider} />
              </React.Fragment>

              <Modal form="building" />
              <Divider className={classes.divider} />

              <ListItem
                tabIndex="0"
                className={classNames(
                  classes.firebase,
                  classes.item,
                  classes.settings
                )}
              >
                Settings
              </ListItem>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemPrimary,
                      textDense: classes.textDense
                    }}
                  >
                    Accessiblity Mode
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    // checked={this.state.checkedDarkMode}
                    onChange={this.handleAccessiblitySwitch}
                    value="checkedB"
                    id='accessiblitySwitch'
                  />
                </ListItem>
              </label>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemPrimary,
                      textDense: classes.textDense
                    }}
                  >
                    Dark Theme
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    checked={this.state.checkedDarkMode}
                    onChange={this.handleDarkThemeSwitch}
                    value="checkedB"
                  />
                </ListItem>
              </label>
              <label>
                <ListItem
                  className={classNames(classes.firebase, classes.item)}
                >
                  <ListItemText
                    classes={{
                      primary: classes.itemPrimary,
                      textDense: classes.textDense
                    }}
                  >
                    Increased Font
                  </ListItemText>
                  <Switch
                    classes={{
                      switchBase: classes.iOSSwitchBase,
                      bar: classes.iOSBar,
                      icon: classes.iOSIcon,
                      iconChecked: classes.iOSIconChecked,
                      checked: classes.iOSChecked
                    }}
                    disableRipple
                    checked={this.state.checkedB}
                    // onChange={this.handleChange('checkedB')}
                    value="checkedB"
                  />
                </ListItem>
              </label>
            </List>
          </Drawer>
        )}
      </div>
    );
  }
}

Navigator.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Navigator);