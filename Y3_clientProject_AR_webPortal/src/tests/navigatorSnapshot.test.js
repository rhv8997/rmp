import React from 'react';
import renderer from 'react-test-renderer';
import Navigator from  '../components/navigator';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';
import {shallow} from 'enzyme';
//medium.com/codeclan/testing-react-with-jest-and-enzyme-20505fec4675

Enzyme.configure({ adapter: new Adapter() });

/*
 This test file uses enzyme and jest to make a snapshot test and check the callback of accessibility switch
 is called whe clicked
  - Created by C1673107
 */


it('Grid should load properly', () => {
    const navigator = renderer.create(<Navigator />).toJSON();
    expect(navigator).toMatchSnapshot();
});

const clickFn = jest.fn();

describe('navigator accessiblity', () => {
    it('switch should callback when clicked', () => {
        const component = shallow(<Navigator onClick={clickFn}/>);
        component.simulate('click');
        expect(clickFn).toHaveBeenCalled();
    });
});