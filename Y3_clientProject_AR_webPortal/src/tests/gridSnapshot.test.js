import React from 'react';
import renderer from 'react-test-renderer';
import Grid from  '../components/grid'

/*
 This test file uses jest to make a snapshot test
  - Created by C1673107
 */

it('Grid should load properly', () => {
    const grid = renderer.create(<Grid />).toJSON();
    expect(grid).toMatchSnapshot();
});