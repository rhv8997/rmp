const coordsScript = require('../scripts/coordMaker');

/*
 This test file tests the methodology of converting a number into an array of x and y coords
  - Created by C1673107
 */

test('test coords script returns array', () => {
    let coords = coordsScript.convertNumberToCoordinates(32);
    expect(Array.isArray(coords)).toBe(true);
});

test('test coords of 100 as string is [10,10]', () => {
    let coords = coordsScript.convertNumberToCoordinates('100');
    expect(coords).toEqual([10,10]);
});

test('test coords of 100 as int is [0,2] because this is how the error should be handled if 3 digit init entered', () => {
    let coords = coordsScript.convertNumberToCoordinates(100);
    expect(coords).toEqual([0,2]);
});

test('test coords of 1 to be [1,1]', () => {
    let coords = coordsScript.convertNumberToCoordinates(1);
    expect(coords).toEqual([1,1]);
});