import React from 'react';
import renderer from 'react-test-renderer';
import Modal from  '../components/modal'
import { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });

/*
 This test file uses enzyme and jest to make a snapshot test and check the modal still loads with no props
  - Created by C1673107
 */

it('modal should load properly', () => {
    const modal = renderer.create(<Modal />).toJSON();
    expect(modal).toMatchSnapshot();
});

it('modal should render correctly with null prop', () => {
    const props = {
            value: null
        },
        modal = mount(<Modal {...props} />);
    expect((modal).prop('value')).toEqual(null);
});


