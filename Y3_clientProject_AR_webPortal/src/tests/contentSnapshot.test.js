import React from 'react';
import renderer from 'react-test-renderer';
import Content from  '../components/content'
import { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });

/*
 This test file uses enzyme and jest to make a snapshot test and check the loading image loads
  - Created by C1673107
 */

it('Main content should load properly', () => {
    const content = renderer.create(<Content />).toJSON();
    expect(content).toMatchSnapshot();
});

it('check prop title by default', () => {
    const appContent = mount(<Content />);
    expect(appContent.find('img'));
});