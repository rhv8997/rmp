/*
  Created by C1673107
  Dependencies: None
  Usage: This script turns a 2 - 3 digit number into an array for coordinates
  It only works for a 10x10 grid
  Status: Finished
 */

module.exports = {
    convertNumberToCoordinates: function(number) {
        // If number a multiple of 10, the x will be 10
        let range = ['10', '20', '30', '40', '50', '60', '70', '80', '90'];
        let x, y;
        console.log(number);
        if (range.includes(number)) {
            x = 10;
            y = Number((String(number).charAt(0)));
        }
        // If number is 100, both coords will be 10
        else {
            if (number === '100') {
                x = 10;
                y = 10;
            }
            else {
                if (Number(number) < 10) {
                    y = 1;
                    x = Number(number);
                }
                else {
                    x = Number(String(number).charAt(1));
                    y = Number(String(number).charAt(0)) + 1;
                }
            }
        }
        return [x,y]
    }
};