/*
  Created by C1673107
  Dependencies:
    - socket.io https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34
    - socket.io-client https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34
  Usage: This api handles the connection from the client to the server to pass data through sockets
  It provides the room data, building data, employees and processes
  Status: Finished
  Libraries:
    - The HTML components are from the Material UI framework - https://material-ui.com/
    - Axios - https://medium.com/@harinilabs/day-5-making-server-request-in-react-with-axios-8e85549caf62
    - Form Data - https://www.npmjs.com/package/form-data
 */

//https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34
import openSocket from 'socket.io-client';
const socket = openSocket('http://ec2-18-130-43-56.eu-west-2.compute.amazonaws.com:4000');

// Room data every 3 seconds
export function subscribeToData(cb) {
    socket.on('data', data => cb(null, data));
    socket.emit('subscribeToData', 3000);
}

// Process data every 3 seconds
export function subscribeToData2(cb) {
    socket.on('data2', data2 => cb(null, data2));
    socket.emit('subscribeToData2', 3000);
}

// Employee data every 1 second
export function subscribeToData3(cb) {
    socket.on('data3', data3 => cb(null, data3));
    socket.emit('subscribeToData3', 1000);
}

// Building data every 1 second
export function subscribeToBuildingData(cb) {
    socket.on('buildingData', buildingData => cb(null, buildingData));
    socket.emit('subscribeToBuildingData', 1000);
}

