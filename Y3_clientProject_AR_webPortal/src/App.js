/*
  Created by C1673107
  Dependencies:
    - navigator.js
    - content.js
    - header.js
    - https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase/
  Usage: This is the main part of app that loads in all the components. It acts as the main styler and the middleman
  for transferring data between components.
  Status: Finished
  Libraries: The HTML components are from the Material UI framework - https://material-ui.com/
 */

// https://github.com/mui-org/material-ui/tree/master/docs/src/pages/premium-themes
import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Hidden from '@material-ui/core/Hidden';
import Navigator from './components/navigator';
import Content from './components/content';
import Header from './components/header';

// https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase.js
let theme = createMuiTheme({
    typography: {
        useNextVariants: true,
        h5: {
            fontWeight: 500,
            fontSize: 26,
            letterSpacing: 0.5,
        },
    },
    palette: {
        primary: {
            light: '#CAC9C5',
            main: '#eaeff1',
            dark: '#F0C105',
            test:  '#fffff2'
        },
        secondary: {
            main: '#000000',
        },
        default: {
            main: '#fffff2',
        }
    },
    shape: {
        borderRadius: 8,
    },
});

// https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase.js
theme = {
    ...theme,
    overrides: {
        MuiDrawer: {
            paper: {
                backgroundColor: '#000000',
            },
        },
        MuiButton: {
            label: {
                textTransform: 'initial',
            },
            contained: {
                boxShadow: 'none',
                '&:active': {
                    boxShadow: 'none',
                },
            },
        },
        MuiTabs: {
            root: {
                marginLeft: theme.spacing.unit,
            },
            indicator: {
                height: 3,
                borderTopLeftRadius: 3,
                borderTopRightRadius: 3,
                backgroundColor: theme.palette.common.white,
            },
        },
        MuiTab: {
            root: {
                textTransform: 'initial',
                margin: '0 16px',
                minWidth: 0,
                [theme.breakpoints.up('md')]: {
                    minWidth: 0,
                },
            },
            labelContainer: {
                padding: 0,
                [theme.breakpoints.up('md')]: {
                    padding: 0,
                },
            },
        },
        MuiIconButton: {
            root: {
                padding: theme.spacing.unit,
            },
        },
        MuiTooltip: {
            tooltip: {
                borderRadius: 4,
            },
        },
        MuiDivider: {
            root: {
                backgroundColor: '#404854',
            },
        },
        MuiListItemText: {
            primary: {
                fontWeight: theme.typography.fontWeightMedium,
            },
        },
        MuiListItemIcon: {
            root: {
                color: 'inherit',
                marginRight: 0,
                '& svg': {
                    fontSize: 20,
                },
            },
        },
        MuiAvatar: {
            root: {
                width: 32,
                height: 32,
            },
        },
    },
    props: {
        MuiTab: {
            disableRipple: true,
        },
    },
    mixins: {
        ...theme.mixins,
        toolbar: {
            minHeight: 48,
        },
    },
};

// https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase.js
const drawerWidth = 256;

const styles = () => ({
    root: {
        display: 'flex',
        minHeight: '100vh',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appContent: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
});

// https://github.com/mui-org/material-ui/blob/master/docs/src/pages/premium-themes/paperbase.js
class App extends React.Component {
    // State to handle dark/accessibilty mode, buildingId prop, form prop and mobile
    state = {
        mobileOpen: false,
        buildingId: 'undefined',
        form: 'undefined',
        style: {
            flex: 1,
            padding: '48px 36px 0',
            background: '#eaeff1',
        },
        textStyle: {
            lightText: {
                color: '#ffffff',
            },
            darkText: {
                color: '#ffffff'
            },
            greyText: {
                color: '#ffffff'
            },
        },
        darkEnabled : false,
        navBackground: '#18202c'
    };

    // Opens mobile nav
    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    // Takes the data from nav and passes to content
    myCallback = (dataFromChild) => {
        this.setState(state => ({
            buildingId: dataFromChild
        }))
    };

    // Takes dark theme boolean from nav to here and then to the needed componenets
    darkTheme = (dataFromChild) => {
        if (dataFromChild) {
            this.setState(state => ({
                style: {
                    flex: 1,
                    padding: '48px 36px 0',
                    background: '#000000',
                    headerBackground: 'secondary',
                },
                darkEnabled: true
            }))
        }
        else {
            this.setState(state => ({
                style: {
                    flex: 1,
                    padding: '48px 36px 0',
                    background: '#eaeff1',
                    headerBackground: 'primary',
                },
                darkEnabled: false
            }))
        }
    };

    // Takes the accessibility boolean from nav to here and then to the needed componenets
    accessiblitiy = (dataFromChild) => {
        if (dataFromChild) {
            this.setState(state => ({
                navBackground: '#f9f1f1',
                style: {
                    flex: 1,
                        padding: '48px 36px 0',
                        background: '#fffff2',
                        headerBackground: 'secondary',
            },
            }))
        }
        else {
            this.setState(state => ({
                navBackground: '#18202c',
                style: {
                    flex: 1,
                    padding: '48px 36px 0',
                    background: '#eaeff1',
                    headerBackground: 'primary',
                },
            }))
        }
    };


    render() {
        const { classes } = this.props;

        return (
            <MuiThemeProvider theme={theme}>
                <div className={classes.root}>
                    <CssBaseline />
                    <nav className={classes.drawer}>
                        <Hidden smUp implementation="js">
                            <Navigator
                                PaperProps={{ style: { width: drawerWidth } }}
                                variant="temporary"
                                open={this.state.mobileOpen}
                                onClose={this.handleDrawerToggle}
                            />
                        </Hidden>
                        <Hidden xsDown implementation="css">
                            <Navigator callBackForDarkMode={this.darkTheme} accessiblityCallBack={this.accessiblitiy} callbackFromParent={this.myCallback} PaperProps={{ style: { width: drawerWidth, backgroundColor: this.state.navBackground } }} />
                        </Hidden>
                    </nav>
                    <div className={classes.appContent}>
                        <Header style={this.state.style} onDrawerToggle={this.handleDrawerToggle} />
                        <main style={this.state.style}>
                            <Content darkMode={this.state.darkEnabled} form={this.state.form} buildingId={this.state.buildingId}/>
                        </main>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);