# Team agreement
## Team 2

## Goal

To satisfy the client by fulfilling their requirements and to work as a team, communicating well and helping one another. 

## Objectives

* Ask the client various questions in order to create an extensive list of requirements
* Communicate with the client often to ensure they like what we have done and to keep track of the requirements. 
* Communicate to teams what you are working on and when you complete it. If you discover any bugs or need help post it to the Help channel on the team's group. 
* Get a full working prototype done by week 4. 
* Provide documentation and tests for code written.

## Roles

* Team Member Roles Scrum master (Lead stand-ups and solve issues that arise) - weekly rotation Project leader 
* Project Leader (Communicate with the client outside of meetings) - Jack Allcock 

## Norms

*  Meeting: Mondays, Tuesdays, Fridays 10-4
*  Work on branches and only push to development branch when all have agreed. Should be done on a Friday morning until week 4 where it should be on by thrusday night. 
*  Communicate via teams 
*  Inform team if you’ll be absent 
*  Check merge requests before submitting • Last merge request to Master branch on Thursday afternoons 
*  Daily stand-ups when meeting 
*  Rehearse presentation Friday mornings before 10am • Update taiga board once tasks has been completed 
*  Use a majority to make decisions; scrum master will make final decision in event of a 50/50 split 
*  Reasons to miss meetings: unforeseen circumstances, illness, prior engagements (e.g. doctor's appointment) 
*  If member misses 3 stand-ups in a row without contact, team will contact you 
*   Each create a prototype, use card sorting to form a final prototype and then stick it to it.
*  Stick to this git flow, master -> dev -> feature: https://www.atlassian.com/git/tutorial s/comparing-workflows/gitflowworkflow 
*  Git messages should be specific, not too long but not too short. Follow these guidelines:
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Separate subject from body with a blank line 
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Limit the subject line to 50 characters 
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Capitalize the subject line
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Do not end the subject line with a period 
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Use the imperative mood in the subject line 
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Wrap the body at 72 characters
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *  Use the body to explain what and why vs. how 

## Member evaluation

* Attendance to meetings
* Sprint performance 
* Communication 
* Effort 
 
## Signed
13/11/2018
Jack Allcock, Daniel Duggan, Reagan Vose, Ieuan Jones 
 
 
