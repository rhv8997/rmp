package com.team2.ar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * In this file are some basic examples of unit tests
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ArApplicationTests {

    private String accessKey = "aa706d18c83ad99a465c82ea1437abcd40f2356f";
    private String secretKey = "31058a4bd68b3c5070f92a290a2f0308ba718d2f";
    private String url = "https://vws.vuforia.com";

    // Test access key is always this
    @Test
    public void testAccessKeyURL() {
        assertEquals(accessKey, "aa706d18c83ad99a465c82ea1437abcd40f2356f");
    }

    // Test secret key is always this
    @Test
    public void testsSecretKeyURL() {
        assertEquals(secretKey, "31058a4bd68b3c5070f92a290a2f0308ba718d2f");
    }

    // Test url key is always this
    @Test
    public void testUrl() {
        assertEquals(url, "https://vws.vuforia.com");
    }

    // Test ability to create image from URL
    @Test
    public void testImageCreation() {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(new URL("https://www.google.co.uk/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(bufferedImage).isInstanceOf(BufferedImage.class);
    }

}
