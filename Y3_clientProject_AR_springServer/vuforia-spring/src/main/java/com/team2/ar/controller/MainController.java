package com.team2.ar.controller;
import com.team2.ar.files.PostNewTarget;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    public MainController() {
    }

    /**
     * This API is called by the Node server
     * @param name: the name param is attached via the Node request, it is the name of the file
     * @param url: the url param is attached via the Node request, it points to the image uploaded onto Node server
     * @param payload: the JSON metadata that needs to be attached
     * @throws URISyntaxException
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(path = "/upload/image/{name}/{url}")
    public void uploadImage(@PathVariable String name, @PathVariable String url, @RequestBody String payload) throws URISyntaxException, ClientProtocolException, IOException, JSONException {
        System.out.println("Image received");
        System.out.println("Name of file:" + " " + name);
        PostNewTarget postNewTarget = new PostNewTarget();
        postNewTarget.setMetaData(payload);
        try {
            // If running on AWS
            BufferedImage bufferedImage = ImageIO.read(new URL("http://ec2-18-130-43-56.eu-west-2.compute.amazonaws.com:4000/images/uploads/"+url));
            // If running locally
            // BufferedImage bufferedImage = ImageIO.read(new URL("http://localhost:4000/images/uploads/"+url));

            postNewTarget.setWidth(bufferedImage.getWidth());
            byte[] image = postNewTarget.extractBytes(bufferedImage);
            postNewTarget.setImage(image);
            postNewTarget.setFileName(name);
            System.out.println("Successfully packaged image, now uploading");
            postNewTarget.postTargetThenPollStatus();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


