package com.team2.ar.files;

// Code provided and developed from Vuforia provided classes - https://developer.vuforia.com/downloads/samples

public interface TargetStatusListener {

	public void OnTargetStatusUpdate(TargetState targetState);
}
