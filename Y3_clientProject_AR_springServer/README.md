![alt text](http://viewmyprocess.co.uk/static/media/logo.f24fe3fb.JPG "RunMyProcess")

# ViewMyProcess SpringBoot Vuforia Server

###### A web application built for RunMyProcess, Fujitsu. 

***
ViewMyProcess is a system that offers managers a unique way of viewing the status of departments in a business. It acheives this through a modern Augmented Reality (AR) UI application. This server allows users to upload images from the [UI portal](https://gitlab.cs.cf.ac.uk/c1673107/react-project-group2.git) to the Vuforia database.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. There are also further instructions on how to deploy the server to Amazon Web Services and start it.

### Prerequisites

What you need to install and how to install them

To download the app you are going to need a git client of your choice. Some good ones are:

- Git Bash - https://gitforwindows.org/
- GitHub Desktop - https://desktop.github.com/

You are also going to an IDE such as [InteliJ](https://www.jetbrains.com/idea/download/#section=windows), [Gradle](https://gradle.org/install/) and [Java JRE and JDK](https://www.java.com/en/download/).

### Installing and running
How to run the application locally

Clone the project using the following command:

```
SSH: git clone git@gitlab.cs.cf.ac.uk:c1673107/spring-server.git
HTTPS: https://gitlab.cs.cf.ac.uk/c1673107/spring-server.git
```

Now that the project is on the system, you can choose to open it with an IDE terminal or just run it via the OS terminal.

Once you have a terminal open, navigate to the root of the project vuforia-spring and use the project gradle wrapper to test, build and boot the server on localhost port 8080.

```
./gradlew test
./gradlew build
./gradlew bootRun
```

## Running the tests

We created some tests to demomnstrate to the client the sort of tests they can run in the future to provide coverage.

Here are the tests we did to assert that URL's and keys used were the correct ones. We also made a test to ensure the process of converting an image from a URL to an image.

```
 // Test access key is always this
    @Test
    public void testAccessKeyURL() {
        assertEquals(accessKey, "aa706d18c83ad99a465c82ea1437abcd40f2356f");
    }

    // Test secret key is always this
    @Test
    public void testsSecretKeyURL() {
        assertEquals(secretKey, "31058a4bd68b3c5070f92a290a2f0308ba718d2f");
    }

    // Test url key is always this
    @Test
    public void testUrl() {
        assertEquals(url, "https://vws.vuforia.com");
    }

    // Test ability to create image from URL
    @Test
    public void testImageCreation() {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(new URL("https://www.google.co.uk/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(bufferedImage).isInstanceOf(BufferedImage.class);
    }
```

We used Junit to test because it comes out of the box with SpringBoot. It has really nice assertions and works really well with Gradle. Some alternatives could include Cucumber to make tests that business owners can understand.

To run the tests, simply use the wrapper:

```
./gradlew test
```

# Tools Used

### Why did we need a SpringBoot server
We needed a SpringBoot server because we wanted to add image targets to Vuforia to add extensibility to our prototype. This gives the client an idea of how this system would work in the real world.

Vuforia only allows uploads to its database via Java, PHP and Python. As a result, we couldn't do it through the node server. 

We chose Java and SpringBoot because we have all used it before and it is incredibly easy and quick to create an API. All the server configuration and boilerplate code is done for us and Beans (XML injection) make it so easy to have multiple services. However, SpringBoot is very heavy. It contains so much inside of it (routing, authentication, services etc). We only needed a simple API. As a result in the future it could be worthy to use Python and Flask as this would be much more lightweight for what we need. A simple PHP script running on a server may be even quicker so there is lots to consider here. Again Flask removes boiler plating which means it would be easier than a PHP script.

### We did we use Gradle

Gradle is a brilliant build tool and much easier to use than Maven due to its groovy language base, not XML. The wrapper means we can put it on version control so people can use it, without needing gradle. This is much easier for servers like AWS where installing gradle with proper rights can be difficult.

### Architecture

We took advantage of the seperation of concerns pattern for this application. We split the application up so that the controller only handles the API routes. It calls services/classes from there to post the image. 
This means that if changes need to be made to the Vuforia upload class, the API itself dosn't need to be changed. They are decoupled.
## Deployment

To deploy the app you need a way of hosting the server.

Create an instance on AWS for the SpringBoot server and open a port security group for 8080.

A tutorial can be followed [here](https://aws.amazon.com/blogs/devops/deploying-a-spring-boot-application-on-aws-using-aws-elastic-beanstalk/)

Once the instance is created, you need to get the project on it. Download an SSH Key from the AWS Instance website.

Then using a terminal, follow the steps below to configure and start the project.

###### SSH Into the server from thge directory you installed the key from a command prompt
```
ssh -i SpringKeyPair.pem ubuntu@your-instance-ip
```

###### Install JRE and JDK and update
```
sudo apt install default-jre
sudo apt install default-jdk
sudo apt-get update
```

###### |Install Git
```
sudo apt-get install git
```
###### Clone the project

```
SSH: git clone git@gitlab.cs.cf.ac.uk:c1673107/spring-server.git
HTTPS: https://gitlab.cs.cf.ac.uk/c1673107/spring-server.git
```

###### CD to the project
```
cd spring-server/vuforia-spring
```

###### Change permissions for gradle
```
chmod +x gradlew
```

###### Build and run via the wrapper
```
sudo ./gradlew build
sudo ./gradlew bootRun
```

## Using the API

The API looks like this:

```
/**
     * This API is called by the Node server
     * @param name: the name param is attached via the Node request, it is the name of the file
     * @param url: the url param is attached via the Node request, it points to the image uploaded onto Node server
     * @param payload: the JSON metadata that needs to be attached
     * @throws URISyntaxException
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(path = "/upload/image/{name}/{url}")
    public void uploadImage(@PathVariable String name, @PathVariable String url, @RequestBody String payload) throws URISyntaxException, ClientProtocolException, IOException, JSONException {
        System.out.println("Image received");
        System.out.println("Name of file:" + " " + name);
        PostNewTarget postNewTarget = new PostNewTarget();
        postNewTarget.setMetaData(payload);
        try {
            // If running on AWS
            // BufferedImage bufferedImage = ImageIO.read(new URL("http://ec2-18-130-43-56.eu-west-2.compute.amazonaws.com:4000/images/uploads/"+url));
            // If running locally
            BufferedImage bufferedImage = ImageIO.read(new URL("http://localhost:4000/images/uploads/"+url));

            postNewTarget.setWidth(bufferedImage.getWidth());
            byte[] image = postNewTarget.extractBytes(bufferedImage);
            postNewTarget.setImage(image);
            postNewTarget.setFileName(name);
            System.out.println("Successfully packaged image, now uploading");
            postNewTarget.postTargetThenPollStatus();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

The API link is: 
``` 
http://awsIP:8080/api/upload/image/{name}/{urlToImage}
or:
http://localhost:8080/api/upload/image/{name}/{urlToImage}
```

The 2 paramters name and url are added in the Node server when the React UI Portal room form is submitted. The name is the name of the image target and the URL points to the image uploaded on the node server.
The API grabs the image from Node and posts to Vuforia via the Java Vuforia implementation which can be found and downloaded from [here (https://developer.vuforia.com/downloads/samples) in the Web Services section (bottom of page).

You also need to add metadata JSON. The request below is an example of how this API is used in Node.
```
 let metaDataToSend = req.file.filename.slice(0, -5);
        let url = 'http://ec2-18-130-225-13.eu-west-2.compute.amazonaws.com:8080/api/upload/image/'+name+'/'+req.file.filename;
        console.log(url);
        request({
            url: url,
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            json: metaDataToSend
        }, function (error, resp, body) {
            console.log(error);
        });
        res.json({
            imageUrl: `images/uploads/${req.file.filename}`
        });
```

## Built With

* [Gradle](https://gradle.org/) - The package manager and build tool 
* [SpringBoot](http://spring.io/projects/spring-boot/) - The Java framework used to create the upload image API
* [Vuforia](https://nodejs.org/en/) - Used for the server to host the API's and send client data

## Versioning

The live version is currently on 1.1

## Authors

* **Jack Allcock** 
* **Reagan Vose**
* **Ieuan Jones**
* **Daniel Duggan**

## Third party packages and software used

| Plugin | Explanation | 
| ------ | ------ |
| spring-boot-starter-web | The Spring starter is the simplist and lightest form of the server API. | 
| lombok | Java library that automatically plugs into your editor. Easy way to use dependency injection for the API's|
| httpclient:4.5 | For making requests |
| apache.commons | Library required for several math functions and HTTP foundations   |
| json | Allow manipulation of JSON |
| junit.jupiter | Allow better testing assertions |

## Acknowledgments

Thank you to Fujitsu and RunMyProcess for allowing us to build this product prototype for them, we hope it has given them a good idea of what they want/is acheviable. 

Thanks to Vuforia for the great classes to help manage targets: https://developer.vuforia.com/resources/dev-guide/adding-target-cloud-database-api




   

